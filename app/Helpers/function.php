<?php

use Carbon\Carbon;
use App\Helpers\Admin;

if (!function_exists('admin')) {
    function admin()
    {
        return new Admin;
    }
}

if (!function_exists('getDateBetweenParams')) {
    /**
     * Get Current Hour.
     *
     * @param string $date_set
     * @return array
     */
    function getDateBetweenParams(string $date_get): array
    {
        /* Convert Parameters $date_get to Date Format */
        $date_set = date('Y-m-d', strtotime($date_get));

        /* Set Date Time With Carbon */
        $today = Carbon::createFromFormat('Y-m-d H:i:s', $date_set . " 20:00:00");

        /* Set Hour Now And Compare With >= 20 */
        $hour = (int) date('H');
        if ($hour >= 20) {
            /* From Today @20 PM To Tomorrow @20 PM */
            return [$today, $today->copy()->addDay()];
        }

        /* From Yesterday @20 PM To Today @20 PM */
        return [$today->copy()->subDay(), $today];
    }
}

if (!function_exists('getCurrentTime')) {
    /**
     * Get Current Date & Time.
     *
     * @return string
     */
    function getCurrentTime(): string
    {
        $date = date('Y-m-d H');
        $minute = (int) date('i');

        if ($minute >= 30) {
            return $date . ":30:00";
        }

        return $date . ":00:00";

    }
}

if (!function_exists('getInputTime')) {
    /**
     * Get List Time Input For Fixed Po.
     *
     * @return array
     */
    function getInputTime(): array
    {
		$input_time = [];
		
        /* TODO: Get Date Range For Chart Data And Collect That */
		$date_current = date('Y-m-d');
        $date_range = getDateBetweenParams($date_current);

		/* TODO: Each Int From 0 to 24 For Get Chart Data By Hours */
        foreach (range(intval('00:00:00'), intval('24:00:00')) as $time) {

            /* TODO: Initial date range when time is between 20:30-23:30 */
            $date_a = $date_range[1]->format('Y-m-d ') . date("H:00:00", mktime($time + 20));
            $date_b = $date_range[1]->format('Y-m-d ') . date("H:30:00", mktime($time + 20));

            /* TODO: Initial date range when time is not between 20:30-23:30 */
            if ($time <= 3) {
                $date_a = $date_range[0]->format('Y-m-d ') . date("H:00:00", mktime($time + 20));
                $date_b = $date_range[0]->format('Y-m-d ') . date("H:30:00", mktime($time + 20));
            }

            $input_time[] = date('Y-m-d H:i:00', strtotime($date_a));
            $input_time[] = date('Y-m-d H:i:00', strtotime($date_b));
        }

        array_pop($input_time);

        return $input_time;
    }
}
