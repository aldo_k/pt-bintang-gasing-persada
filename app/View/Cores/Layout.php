<?php

namespace App\View\Cores;

use Illuminate\View\Component;

class Layout extends Component {

    public $adminUser;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct() {
      $this->adminUser = auth()->user();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('template::layouts.app');
    }
}
