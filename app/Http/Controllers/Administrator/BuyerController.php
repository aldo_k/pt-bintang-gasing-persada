<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Exceptions\AdminException;
use App\DataTables\BuyerDatatables;
use App\Http\Controllers\Controller;
use App\Repositories\BuyerRepository;

class BuyerController extends Controller
{

    protected $repository;

    public function __construct(BuyerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        admin()->allow('administrator.buyer.index');

        return BuyerDatatables::view();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        admin()->allow('administrator.buyer.create');

        return view('administrator.buyer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        admin()->allow('administrator.buyer.create');

        $request->validate([
            'name' => ['required'],
            'target' => ['required', 'min:1', 'max:99'],
            'usl' => ['required', 'min:1', 'max:99'],
            'lsl' => ['required', 'min:1', 'max:99'],
        ]);

        try {

            $request->merge(['created_by' => auth()->id()]);

            $this->repository->createBuyer($request);
            session()->flash('success', [
                'Buyer has been created sucessfully',
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage(),
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buyer = $this->repository->getModel()->findOrFail($id);
        return response()->json($buyer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        admin()->allow('administrator.buyer.update');

        $data['buyer'] = $this->repository->getModel()->findOrFail($id);
        return view('administrator.buyer.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        admin()->allow('administrator.buyer.update');

        $request->validate([
            'name' => ['required'],
            'target' => ['required', 'min:1', 'max:99'],
            'usl' => ['required', 'min:1', 'max:99'],
            'lsl' => ['required', 'min:1', 'max:99'],
        ]);

        try {
            $request->merge(['updated_by' => auth()->id()]);

            $this->repository->updateBuyer($request, $id);
            session()->flash('success', [
                'Update has been sucessfully',
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        admin()->allow('administrator.buyer.destroy');

        try {
            $this->repository->getModel()->findOrFail($id)->delete();
            session()->flash('success', [
                'Delete has been sucessfully',
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage(),
            ]);
        }
    }
}
