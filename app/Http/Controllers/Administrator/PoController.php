<?php

namespace App\Http\Controllers\Administrator;

use App\Models\Buyer;
use Illuminate\Http\Request;
use App\DataTables\PoDatatables;
use App\Exceptions\AdminException;
use App\Repositories\PoRepository;
use App\Http\Controllers\Controller;

class PoController extends Controller
{

    protected $repository;

    public function __construct(PoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        admin()->allow('administrator.po.index');

        return PoDatatables::view();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        admin()->allow('administrator.po.create');

        $data['buyers'] = Buyer::all();

        $data['current_time'] = getCurrentTime();

        return view('administrator.po.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        admin()->allow('administrator.po.create');

        $request->validate([
            'po_a' => ['nullable', 'min:1', 'max:99'],
            'po_b' => ['nullable', 'min:1', 'max:99'],
            'target' => ['required', 'min:1', 'max:99'],
            'usl' => ['required', 'min:1', 'max:99'],
            'lsl' => ['required', 'min:1', 'max:99'],
        ]);

        try {
            $current_time = getCurrentTime();

            $isPoExist = $this->repository->findPoByTime($current_time);

            if ($isPoExist) {
                return redirect()->back()->withErrors([
                    'Create Failed. Po has been existing!',
                ]);
            }

            $request->merge([
                'po_a_temp' => $request->po_a,
                'po_b_temp' => $request->po_b,
                'report_time' => $current_time,
                'created_by' => auth()->id(),
            ]);

            $this->repository->createPo($request);

            session()->flash('success', [
                'Po has been created sucessfully',
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage(),
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('administrator.po.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        admin()->allow('administrator.po.update');

        $data['buyers'] = Buyer::all();
        $data['po'] = $this->repository->getModel()->findOrFail($id);

        return view('administrator.po.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        admin()->allow('administrator.po.update');

        $request->validate([
            'po_a' => ['nullable', 'min:1', 'max:99'],
            'po_b' => ['nullable', 'min:1', 'max:99'],
            'target' => ['required', 'min:1', 'max:99'],
            'usl' => ['required', 'min:1', 'max:99'],
            'lsl' => ['required', 'min:1', 'max:99'],
        ]);

        try {

            $request->merge(['updated_by' => auth()->id()]);

            $this->repository->updatePo($request, $id);
            session()->flash('success', [
                'Update has been sucessfully',
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        admin()->allow('administrator.po.destroy');

        try {
            $this->repository->getModel()->findOrFail($id)->delete();
            session()->flash('success', [
                'Delete has been sucessfully',
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage(),
            ]);
        }
    }

    /**
     * Get Current Date & Time.
     *
     * @return string
     */
    private function getCurrentTime()
    {
        $date = date('Y-m-d H');
        $minute = (int) date('i');

        if ($minute >= 30) {
            return $date . ":30:00";
        }

        return $date . ":00:00";

    }
}
