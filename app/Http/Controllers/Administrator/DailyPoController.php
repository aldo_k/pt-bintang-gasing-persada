<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Exceptions\AdminException;
use App\Http\Controllers\Controller;
use App\Repositories\DailyReportRepository;

class DailyPoController extends Controller
{

    protected $repository;

    public function __construct(DailyReportRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(string $date = 'today')
    {
        return view('administrator.daily-po.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('administrator.daily-po.index');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        admin()->allow('administrator.po.create');

        $request->validate([
            'date_report' => ['required'],
        ]);

        try {
            $this->repository->updateOrCreateDailyReport($request, $request->date_report);

            session()->flash('success', [
                'Daily Report has been updated sucessfully',
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage(),
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('administrator.daily-po.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $date
     * @return \Illuminate\Http\Response
     */
    public function showByDate($date)
    {
        $daily_report = $this->repository->findByDate($date);
        return response()->json($daily_report);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('administrator.daily-po.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect()->route('administrator.daily-po.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect()->route('administrator.daily-po.index');
    }
}
