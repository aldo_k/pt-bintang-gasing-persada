<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RoleRepository;
use App\Exceptions\AdminException;
use App\DataTables\RoleDatatables;

class RoleController extends Controller {

    protected $repository, $gate_path;

    public function __construct(RoleRepository $repository) {
        $this->repository = $repository;
        $this->gate_path = 'administrator.access.role';
        $this->view_path = 'administrator.role';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        admin()->allow("{$this->gate_path}.index");

        return RoleDatatables::view();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // admin()->allow("{$this->gate_path}.create");

        return view("{$this->view_path}.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        admin()->allow("{$this->gate_path}.create");

        $request->validate([
            'name' => ['required']
        ]);

        try {
            $this->repository->createRole($request);
            session()->flash('success', [
                'Role has been created sucessfully'
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect()->route("{$this->gate_path}.index");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        admin()->allow("{$this->gate_path}.update");

        $data['role'] = $this->repository->getModel()->findOrFail($id);
        return view("{$this->view_path}.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        admin()->allow("{$this->gate_path}.update");

        $request->validate([
            'name' => ['required']
        ]);
        try {
            $this->repository->updateRole($request, $id);
            session()->flash('success', [
                'Update has been sucessfully'
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        admin()->allow("{$this->gate_path}.destroy");

        try {
            $this->repository->getModel()->findOrFail($id)->delete();
            session()->flash('success', [
                'Delete has been sucessfully'
            ]);
            return redirect()->back();
        } catch (AdminException $e) {
            return redirect()->back()->withErrors([
                $e->getMessage()
            ]);
        }
    }
}
