<?php

namespace App\Http\Controllers\Administrator;

use Carbon\Carbon;
use App\Repositories\PoRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class ChartController extends Controller
{

    protected $repository;

    public function __construct(PoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(string $date = 'today')
    {
        if ($date = 'today') {
            $date = date('Y-m-d');
        }

        $data = ['date' => $date];

        return view('administrator.chart.real-po', $data);
    }

    /**
     * Display a json of the resource po by date for chart.
     *
     * @param string $date
     * @return \Illuminate\Http\Response
     */
    public function getChart(string $date)
    {
        $date_params = date('Y-m-d', strtotime($date));
        /* TODO: Get Date Range For Chart Data And Collect That */
        $date_range = getDateBetweenParams($date_params);
        $listPo = $this->repository->chartByDateRange($date_range);

        $collectPo = collect($listPo);

        /* TODO: Po (A) and Po (B) Min Max Value */
        $po_a_min = $collectPo->where('po_a', '!=', 0)->min('po_a');
        $po_a_max = $collectPo->where('po_a', '!=', 0)->max('po_a');

        $po_b_min = $collectPo->where('po_b', '!=', 0)->min('po_b');
        $po_b_max = $collectPo->where('po_b', '!=', 0)->max('po_b');

        /* TODO: Po (A) and Po (B) Temporary Min Max Value */
        $po_a_min_temp = $collectPo->where('po_a_temp', '!=', 0)->min('po_a_temp');
        $po_a_max_temp = $collectPo->where('po_a_temp', '!=', 0)->max('po_a_temp');

        $po_b_min_temp = $collectPo->where('po_b_temp', '!=', 0)->min('po_b_temp');
        $po_b_max_temp = $collectPo->where('po_b_temp', '!=', 0)->max('po_b_temp');

        $min_lsl = $collectPo->where('lsl', '!=', 0)->min('lsl');

        /* TODO: Initial po & po_temp data */
        $po = [];
        $po_temp = [];

        foreach ($listPo as $key => $data) {
            /* TODO: Po Report By Hours */
            $po[$data['report_time']] = [
                date('H:i', strtotime($data['report_time'])),
                $data['po_a']==0 ? null : $data['po_a'],
                $data['po_b']==0 ? null : $data['po_b'],
                $data['target']==0 ? null : $data['target'],
                $data['ucl1']==0 ? null : $data['ucl1'],
                $data['lcl1']==0 ? null : $data['lcl1'],
                $data['usl']==0 ? null : $data['usl'],
                $data['lsl']==0 ? null : $data['lsl'],
            ];

            /* TODO: Po Temporary Report By Hours */
            $po_temp[$data['report_time']] = [
                date('H:i', strtotime($data['report_time'])),
                $data['po_a_temp']==0 ? null : $data['po_a_temp'],
                $data['po_b_temp']==0 ? null : $data['po_b_temp'],
                $data['target']==0 ? null : $data['target'],
                $data['ucl1']==0 ? null : $data['ucl1'],
                $data['lcl2']==0 ? null : $data['lcl2'],
                $data['usl']==0 ? null : $data['usl'],
                $data['lsl']==0 ? null : $data['lsl'],
            ];
        }

        /* TODO: Initial Po & Po Temporary Current */
        $po_current = null;
        $po_current_temp = null;

        $current_time = getCurrentTime();
        if (array_key_exists($current_time, $po)) {
            /* TODO: Po & Po Temporary Currently */
            $po_current = $po[$current_time];
            $po_current_temp = $po_temp[$current_time];
        }

        /* TODO: Initial Array List Time For Chart */
        $chart_data = [];
        $chart_data_temp = [];

        $last_target = $last_ucl_a = $last_lcl_b = $usl = $lsl = null;
        $last_target_temp = $last_ucl_a_temp = $last_lcl_b_temp = $usl_temp = $lsl_temp = null;

        /* TODO: Each Int From 0 to 24 For Get Chart Data By Hours */
        foreach (range(intval('00:00:00'), intval('24:00:00')) as $time) {

            /* TODO: Initial date range when time is between 20:30-23:30 */
            $date_a = $date_range[1]->format('Y-m-d ') . date("H:00:00", mktime($time + 20));
            $date_b = $date_range[1]->format('Y-m-d ') . date("H:30:00", mktime($time + 20));

            /* TODO: Initial date range when time is not between 20:30-23:30 */
            if ($time <= 3) {
                $date_a = $date_range[0]->format('Y-m-d ') . date("H:00:00", mktime($time + 20));
                $date_b = $date_range[0]->format('Y-m-d ') . date("H:30:00", mktime($time + 20));
            }

            if (array_key_exists($date_a, $po)) {
                $chart_data[] = $po[$date_a];
                $last_target = $po[$date_a][3];
                $last_ucl_a = $po[$date_a][4];
                $last_lcl_b = $po[$date_a][5];
                $usl = $po[$date_a][6];
                $lsl = $po[$date_a][7];
            } else {
                $chart_data[] = [date('H:i', strtotime($date_a)), null, null, $last_target, $last_ucl_a, $last_lcl_b, $usl, $lsl];
            }

            if (array_key_exists($date_a, $po_temp)) {
                $chart_data_temp[] = $po_temp[$date_a];
                $last_target_temp = $po_temp[$date_a][3];
                $last_ucl_a_temp = $po_temp[$date_a][4];
                $last_lcl_b_temp = $po_temp[$date_a][5];
                $usl_temp = $po_temp[$date_a][6];
                $lsl_temp = $po_temp[$date_a][7];
            } else {
                $chart_data_temp[] = [date('H:i', strtotime($date_a)), null, null, $last_target_temp, $last_ucl_a_temp, $last_lcl_b_temp, $usl_temp, $lsl_temp];
            }

            if (array_key_exists($date_b, $po)) {
                $chart_data[] = $po[$date_b];
                $last_target = $po[$date_b][3];
                $last_ucl_a = $po[$date_b][4];
                $last_lcl_b = $po[$date_b][5];
                $usl = $po[$date_b][6];
                $lsl = $po[$date_b][7];

            } else {
                $chart_data[] = [date('H:i', strtotime($date_b)), null, null, $last_target, $last_ucl_a, $last_lcl_b, $usl, $lsl];
            }

            if (array_key_exists($date_b, $po_temp)) {
                $chart_data_temp[] = $po_temp[$date_b];
                $last_target_temp = $po_temp[$date_b][3];
                $last_ucl_a_temp = $po_temp[$date_b][4];
                $last_lcl_b_temp = $po_temp[$date_b][5];
                $usl_temp = $po_temp[$date_b][6];
                $lsl_temp = $po_temp[$date_b][7];
            } else {
                $chart_data_temp[] = [date('H:i', strtotime($date_b)), null, null, $last_target_temp, $last_ucl_a_temp, $last_lcl_b_temp, $usl_temp, $lsl_temp];
            }
        }

        /* Remove Last Array */
        array_pop($chart_data);
        array_pop($chart_data_temp);

        $silent_alarm_time = Cache::get('silent_alarm_time', 'Silent Alarm Time Not Exist!');

        $collection_po = collect([$po_a_max, $po_b_max, $po_a_min, $po_b_min, $lsl, $min_lsl]);
        $usl = $collection_po->max();
        $lsl = $collection_po->min();

        $collection_po_temp = collect([$po_a_max_temp, $po_b_max_temp, $po_a_min_temp, $po_b_min_temp, $lsl_temp, $min_lsl]);
        $usl_temp = $collection_po_temp->max();
        $lsl_temp = $collection_po_temp->min();

        return response()->json([
            'date' => $date_range,
            'silent_alarm_time' => $silent_alarm_time,

            'max_y' => ($usl + 1),
            'min_y' => ($lsl - 1),
            'current' => $po_current,
            'chart_data' => $chart_data,

            'max_y_temp' => ($usl_temp + 1),
            'min_y_temp' => ($lsl_temp - 1),
            'current_temp' => $po_current_temp,
            'chart_data_temp' => $chart_data_temp,

        ]);

    }

    /**
     * Set Silent Alarm With Cache Laravel.
     *
     * @return \Illuminate\Http\Response
     */
    public function setSilentAlarmTime(string $time)
    {
        Cache::put('silent_alarm_time', $time, now()->addMinutes(60));

        return response()->json(['message' => "Silent Alarm At {$time} Done!"]);
    }
}
