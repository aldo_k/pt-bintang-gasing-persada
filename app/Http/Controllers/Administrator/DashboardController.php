<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(string $date = 'today')
    {
        if ($date = 'today') {
            $date = date('Y-m-d');
        }

        $data = ['date' => $date];

        return view('administrator.dashboard.index', $data);
    }

}
