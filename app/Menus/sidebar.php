<?php

return [
    [
        'gate' => 'administrator.chart.index',
        'name' => 'Report SPC Po',
        'description' => 'Report SPC Po',
        'route' => ['administrator.chart.index', []],
        'isActive' => 'chart*',
        'icon' => 'fas fa-chart-line mr-2',
        'id' => '',
        'gates' => [],
        // 'submenus' => [],
    ],
    [
        'gate' => 'administrator.buyer.index',
        'name' => 'Buyers',
        'description' => 'Buyer Menu',
        'route' => ['administrator.buyer.index', []],
        'isActive' => 'buyer*',
        'icon' => 'fas fa-user-tie mr-2',
        'id' => '',
        'gates' => [
            [
                'gate' => 'administrator.buyer.create',
                'title' => 'Create Buyer',
                'description' => 'User can create new Buyer',
            ],
            [
                'gate' => 'administrator.buyer.update',
                'title' => 'Update Buyer',
                'description' => 'User can update Buyer',
            ],
            [
                'gate' => 'administrator.buyer.destroy',
                'title' => 'Delete Buyer',
                'description' => 'User can delete Buyer',
            ],
        ],
        // 'submenus' => [],
    ],
    [
        'gate' => 'administrator.po.index',
        'name' => 'Input Po',
        'description' => 'Po Menu',
        'route' => ['administrator.po.index', []],
        'isActive' => 'po*',
        'icon' => 'fas fa-calendar-alt mr-2',
        'id' => '',
        'gates' => [
            [
                'gate' => 'administrator.po.create',
                'title' => 'Create Po',
                'description' => 'User can create new Po',
            ],
            [
                'gate' => 'administrator.po.update',
                'title' => 'Update Po',
                'description' => 'User can update Po',
            ],
            [
                'gate' => 'administrator.po.destroy',
                'title' => 'Delete Po',
                'description' => 'User can delete Po',
            ],
        ],
        // 'submenus' => [],
    ],
    [
        'gate' => 'administrator.fixed-po.index',
        'name' => 'Data Po Fixed',
        'description' => 'Data Po Fixed',
        'route' => ['administrator.fixed-po.index', []],
        'isActive' => 'fixed-po*',
        'icon' => 'fas fa-calendar-alt mr-2',
        'id' => '',
        'gates' => [
            [
                'gate' => 'administrator.fixed-po.create',
                'title' => 'Create Po',
                'description' => 'User can create new Po',
            ],
            [
                'gate' => 'administrator.fixed-po.update',
                'title' => 'Update Po',
                'description' => 'User can update Po',
            ],
        ],
        // 'submenus' => [],
    ],

    [
        'gate' => 'administrator.daily-po.index',
        'name' => 'Daily Po',
        'description' => 'Daily Po',
        'route' => ['administrator.daily-po.index', []],
        'isActive' => 'daily-po*',
        'icon' => 'fas fa-calendar-alt mr-2',
        'id' => '',
        'gates' => [],
        // 'submenus' => [],
    ],

    [
        'gate' => 'administrator.account',
        'name' => 'Account',
        'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        'route' => null,
        'isActive' => null,
        /**
         * You can use fontawesome or svg file, the svg file is viewable in the resources/assets/icons directory
         * Example to Custom SVG file 'icon' => 'somefolder.customsvgfile' --> resources/assets/icons/somefolder/customsvgfile.svg
         * Exampe for fontawesome 'icon' => 'fas fa-user',
         */
        'icon' => 'user-group',
        'id' => '',
        'gates' => [],
        'submenus' => [

            [
                'gate' => 'administrator.account.admin.index',
                'name' => 'User Admin',
                'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
                /**
                 * Declaration route Example
                 * ['administrator.account.admin.show', ['uuid-uuid-uuid', 'foo' => 'bar']] --> https://domain.com/administrator/account/admin/uuid-uuid-uuid?foo=bar
                 */
                'route' => ['administrator.account.admin.index', []],
                'isActive' => 'account/admin*',
                'id' => '',
                'gates' => [
                    [
                        'gate' => 'administrator.account.admin.create',
                        'title' => 'Create admin',
                        'description' => 'User can create new admin',
                    ],
                    [
                        'gate' => 'administrator.account.admin.update',
                        'title' => 'Update admin',
                        'description' => 'User can update admin',
                    ],
                    [
                        'gate' => 'administrator.account.admin.destroy',
                        'title' => 'Delete account',
                        'description' => 'User can delete account',
                    ],
                ],
            ],

        ],
    ],

    [
        'gate' => 'administrator.access',
        'name' => 'Access',
        'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        'route' => null,
        'isActive' => null,
        'icon' => 'lock-open',
        'id' => '',
        'gates' => [],
        'submenus' => [

            [
                'gate' => 'administrator.access.role.index',
                'name' => 'Role',
                'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
                'route' => ['administrator.access.role.index', []],
                'isActive' => 'access/role*',
                'id' => '',
                'gates' => [
                    [
                        'gate' => 'administrator.access.role.create',
                        'title' => 'Create Role',
                        'description' => 'User can create new role',
                    ],
                    [
                        'gate' => 'administrator.access.role.update',
                        'title' => 'Update Role',
                        'description' => 'User can update role',
                    ],
                    [
                        'gate' => 'administrator.access.role.destroy',
                        'title' => 'Delete Role',
                        'description' => 'User can delete role',
                    ],
                ],
            ],

            [
                'gate' => 'administrator.access.permission.index',
                'name' => 'Permission',
                'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
                'route' => ['administrator.access.permission.index', []],
                'isActive' => 'access/permission*',
                'id' => '',
                'gates' => [
                    [
                        'gate' => 'administrator.access.permission.show',
                        'title' => 'Views detail Permission',
                        'description' => 'User can view detail for all permission',
                    ],
                    [
                        'gate' => 'administrator.access.permission.assign',
                        'title' => 'Assign Permission',
                        'description' => 'User can assign for all permission',
                    ],

                ],
            ],
        ],
    ],

    [
        'gate' => 'administrator.system',
        'name' => 'System',
        'description' => 'System application control',
        'route' => null,
        'isActive' => null,
        'icon' => 'cog',
        'id' => '',
        'gates' => [],
        'submenus' => [
            [
                'gate' => 'administrator.system.activity.index',
                'name' => 'User Activity',
                'description' => 'List of User activity',
                'route' => ['administrator.system.activity.index', []],
                'isActive' => 'system/activity*',
                'id' => '',
                'gates' => [
                    [
                        'gate' => 'administrator.system.activity.delete',
                        'title' => 'Delete',
                        'description' => 'Delete log activity after 7 days',
                    ],
                ],
            ],

            [
                'gate' => 'administrator.system.log.index',
                'name' => 'System Log',
                'description' => 'Display for Admin error log',
                'route' => ['administrator.system.log.index', []],
                'isActive' => 'system/log*',
                'id' => '',
                'gates' => [],
            ],
        ],
    ],
];
