<?php

namespace App\DataTables;

use App\Models\Po;
use App\DataTables\Datatables;
use App\Contracts\DataTablesInterface;

class PoDatatables extends Datatables implements DataTablesInterface {

  public function render() {

      /**
       * Data from controller
       */
      $data = self::$data;

      return $this->eloquent(
        Po::query()->with(['user_created', 'user_updated'])
      )
      ->editColumn('user_created.name', function($item) {
        return $item->user_created->name;
      })
      ->editColumn('user_updated.name', function($item) {
        return $item->user_updated ? $item->user_updated->name : '-';
      })
      ->addColumn('action', function($item) {
        return view('template::table.action', [
          'show' => null,
          'edit' => [
            'gate' => 'administrator.po.update',
            'url' => route('administrator.po.edit', [$item->id, 'back' => request()->fullUrl()])
          ],
          'destroy' => [
            'gate' => 'administrator.po.destroy',
            'url' => route('administrator.po.destroy', [$item->id, 'back' => request()->fullUrl()]),
          ]
        ]);
      })
      ->escapeColumns([])
      ->make(true);
    }

    /**
     * Datatables Option
     */
    public function options() {

      /**
       * Data from controller
       */
      $data = self::$data;

      return [
        'title' => 'Input Po',
        'buttons' => view('administrator.po._partials._topButton'),
        'fields' => [ __('Time SPC Po'), __('Po (A)'), __('Po (B)'), __('Target'), __('USL'), __('LSL'), __('Created By'), __('Updated By'), __('Created At'), __('Updated At'), __('Action')],
        'options' => [
          'processing' => true,
          'serverSide' => true,
          'ajax' => request()->fullurl(),
          'columns' => [
            ['data' => 'report_time', 'class' => 'text-center'],
            ['data' => 'po_a', 'class' => 'text-center'],
            ['data' => 'po_b', 'class' => 'text-center'],
            ['data' => 'target'],
            ['data' => 'usl', 'class' => 'text-center'],
            ['data' => 'lsl', 'class' => 'text-center'],
            ['data' => 'user_created.name', 'orderable' => false],
            ['data' => 'user_updated.name', 'orderable' => false],
            ['data' => 'created_at'],
            ['data' => 'updated_at'],
            ['data' => 'action', 'class' => 'text-center', 'orderable' => false]
          ]
        ]
      ];

    }

  }