<?php

namespace App\DataTables;

use App\Models\Buyer;
use App\DataTables\Datatables;
use App\Contracts\DataTablesInterface;

class BuyerDatatables extends Datatables implements DataTablesInterface {

  public function render() {

      /**
       * Data from controller
       */
      $data = self::$data;

      return $this->eloquent(
        Buyer::query()->with(['user_created', 'user_updated'])
      )
      ->editColumn('user_created.name', function($item) {
        return $item->user_created->name;
      })
      ->editColumn('user_updated.name', function($item) {
        return $item->user_updated ? $item->user_updated->name : '-';
      })
      ->addColumn('action', function($item) {
        return view('template::table.action', [
          'show' => null,
          'edit' => [
            'gate' => 'administrator.buyer.update',
            'url' => route('administrator.buyer.edit', [$item->id, 'back' => request()->fullUrl()])
          ],
          'destroy' => [
            'gate' => 'administrator.buyer.destroy',
            'url' => route('administrator.buyer.destroy', [$item->id, 'back' => request()->fullUrl()]),
          ]
        ]);
      })
      ->escapeColumns([])
      ->make(true);
    }

    /**
     * Datatables Option
     */
    public function options() {

      /**
       * Data from controller
       */
      $data = self::$data;

      return [
        'title' => 'Buyer',
        'buttons' => view('administrator.buyer._partials._topButton'),
        'fields' => [ __('Name'), __('Target'), __('USL (Default)'), __('LSL (Default)'), __('Created By'), __('Updated By'), __('Created At'), __('Updated At'), __('Action')],
        'options' => [
          'processing' => true,
          'serverSide' => true,
          'ajax' => request()->fullurl(),
          'columns' => [
            ['data' => 'name'],
            ['data' => 'target'],
            ['data' => 'usl'],
            ['data' => 'lsl'],
            ['data' => 'user_created.name', 'orderable' => false],
            ['data' => 'user_updated.name', 'orderable' => false],
            ['data' => 'created_at'],
            ['data' => 'updated_at'],
            ['data' => 'action', 'class' => 'text-center', 'orderable' => false]
          ]
        ]
      ];

    }

  }