<?php

namespace App\Listeners;

use App\Exceptions\AdminException;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\AdminLogable;

class LoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Login $event) {

        $user = $event->user;

        $instance = app(config('admin.user'));
        if(! ($user instanceof $instance)) {
            return;
        }

        if(is_null($user->role)) {
            return;
        }

        try {

            $new_data = [
                'ip' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            ];

            AdminLogable::create([
                'user_id' => $user->id,
                'new_data' => $new_data,
                'logable_type' => get_class($user),
                'logable_id' => $user->id,
                'old_data' => [],
                'type' => 'login',
            ]);
        } catch (AdminException $e) {}
    }
}
