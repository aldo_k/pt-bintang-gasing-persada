<?php

namespace App\Repositories;

use App\Models\Buyer;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;
use App\Contracts\MasterRepositoryInterface;

class BuyerRepository extends Repository implements MasterRepositoryInterface {

  public function __construct(Buyer $model) {
    parent::__construct($model);
  }

  /**
   * Update Buyer
   *
   * @param Request $request
   * @param [Model] $role
   * @return Void
   */
  public function updateBuyer(Request $request, $id) {
    $this->model->findOrFail($id)->update($request->all());
  }

  /**
   * Create New Buyer
   *
   * @param Request $request
   * @return void
   */
  public function createBuyer(Request $request) {
    $this->model->create($request->all());
  }

}