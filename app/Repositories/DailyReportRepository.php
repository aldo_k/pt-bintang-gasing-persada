<?php

namespace App\Repositories;

use App\Models\DailyReport;
use Illuminate\Http\Request;
use App\Contracts\MasterRepositoryInterface;

class DailyReportRepository extends Repository implements MasterRepositoryInterface
{

    public function __construct(DailyReport $model)
    {
        parent::__construct($model);
    }

    /**
     * Create Or Update Daily Report By Date
     *
     * @param Request $request
     * @param string $date
     * @return Void
     */
    public function updateOrCreateDailyReport(Request $request, $date)
    {
        $this->model->updateOrCreate(
            ['date_report' => request('date_report')],
            $request->all()
        );
    }

    /**
     * Find Existing Daily Report By Date
     *
     * @param string $date_report
     * @return bool
     */
    public function findByDate(string $date_report)
    {
        return $this->model->where('date_report', $date_report)->firstOrFail();
    }
}
