<?php

namespace App\Repositories;

use App\Models\Po;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Contracts\MasterRepositoryInterface;

class PoRepository extends Repository implements MasterRepositoryInterface
{

    public function __construct(Po $model)
    {
        parent::__construct($model);
    }

    /**
     * Update Po
     *
     * @param Request $request
     * @param [Model] $role
     * @return Void
     */
    public function updatePo(Request $request, $id)
    {
        $this->model->findOrFail($id)->update($request->all());
    }

    /**
     * Create New Po
     *
     * @param Request $request
     * @return void
     */
    public function createPo(Request $request)
    {
        $this->model->create($request->all());
    }

    /**
     * Find Existing Po By Time Input
     *
     * @param string $report_time
     * @return bool
     */
    public function findPoByTime(string $report_time)
    {
        return $this->model->where('report_time', $report_time)->first();
    }

    /**
     * Find Existing Po By Time Input
     *
     * @param array $report_time
     * @return bool
     */
    public function chartByDateRange(array $report_time)
    {
        return $this->model
            ->select(
                'report_time',
                DB::raw('COALESCE(po_a, 0) as po_a'),
                DB::raw('COALESCE(po_b, 0) as po_b'),
                DB::raw('COALESCE(po_a_temp, 0) as po_a_temp'),
                DB::raw('COALESCE(po_b_temp, 0) as po_b_temp'),
                DB::raw('COALESCE(target, 0) as target'),
                DB::raw('COALESCE(lsl, 0) as lsl'),
                DB::raw('COALESCE(usl, 0) as usl'),
                DB::raw('COALESCE(target, 0)+2 AS ucl1'),
                DB::raw('COALESCE(target, 0)+2 AS ucl2'),
                DB::raw('COALESCE(target, 0)-1 AS lcl1'),
                DB::raw('COALESCE(target, 0)-2 AS lcl2'),
            )
            ->whereBetween('report_time', $report_time)
            ->orderBy('report_time', 'ASC')
            ->get()->toArray();
    }

}
