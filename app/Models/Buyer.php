<?php

namespace App\Models;

use App\Models\AdminLogableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Buyer extends Model
{
    use HasFactory, AdminLogableTrait;

    protected $table = 'buyers';

    protected $fillable = [
        'name', 'target', 'usl', 'lsl', 'status', 'created_by', 'updated_by'
    ];

    public function user_created() {
        return $this->hasOne(config('admin.user'), 'id', 'created_by');
    }

    public function user_updated() {
        return $this->hasOne(config('admin.user'), 'id', 'updated_by');
    }

}
