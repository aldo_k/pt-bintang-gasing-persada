<?php

namespace App\Models;

use App\Models\AdminLogable as AdminLogableModel;

trait AdminLogableTrait {

  public function admin_logable() {
    return $this->morphMany(AdminLogableModel::class, 'logable');
  }

  public static function createLog($model, $event) {

    if(auth()->guard(config('admin.auth.guard'))->guest()){
      return;
    }

    $user = auth()->guard(config('admin.auth.guard'))->user();


    $instance = app( config('admin.user') );
    if(!($user instanceof  $instance )) {
      return;
    }

    $model->admin_logable()->create([
      'user_id' => $user->id,
      'type' => $event,
      'old_data' => $model->getOriginal(),
      'new_data' => $model
    ]);

  }

  protected static function booted() {

    self::created(function($model) {
      self::createLog($model, 'create');
    });

    self::updated(function($model) {
      self::createLog($model, 'edit');
    });

    self::deleted(function($model) {
      self::createLog($model, 'delete');
    });

  }
}