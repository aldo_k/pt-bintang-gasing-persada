<?php

namespace App\Models;

use App\Models\AdminLogableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Po extends Model
{
    use HasFactory, AdminLogableTrait;

    protected $table = 'po';

    protected $fillable = [
        'po_a', 'po_a_temp', 'po_b', 'po_b_temp', 'target', 'usl', 'lsl', 'report_time', 'created_by', 'updated_by'
    ];

    public function user_created() {
        return $this->hasOne(config('admin.user'), 'id', 'created_by');
    }

    public function user_updated() {
        return $this->hasOne(config('admin.user'), 'id', 'updated_by');
    }

}
