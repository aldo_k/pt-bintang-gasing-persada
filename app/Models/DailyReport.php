<?php

namespace App\Models;

use App\Models\AdminLogableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DailyReport extends Model
{
    use HasFactory, AdminLogableTrait;

    protected $table = 'daily_report';

    protected $fillable = [
        'cooper_a', 'cooper_b', 'block_a', 'block_b', 'drc_a', 'drc_b', 'po_a', 'po_b', 'pri_a', 'pri_b', 'umur_jemur_a', 'umur_jemur_b', 'kamar_a', 'kamar_b', 'target_a', 'target_b', 'date_report', 'status'
    ];

}
