<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\AdminLogableTrait;

class Role extends Model {

    use HasFactory, AdminLogableTrait;

    protected $table = 'admin_roles';

    protected $fillable = [
        'name',
        'gates'
    ];

    protected $casts = [
        'gates' => 'array'
    ];
}
