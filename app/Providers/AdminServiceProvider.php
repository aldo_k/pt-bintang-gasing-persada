<?php

namespace App\Providers;

use App\Helpers\Menu;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Components\Cores\Alert;
/**
 * Components
 */
use App\Components\Cores\Layout;
use App\Components\Menus\Sidebar;
use App\Commands\DataTablesCommand;
use App\Components\Components\Card;

use Illuminate\Contracts\Auth\Authenticatable;
use App\Components\Components\Input;

use App\Components\Cores\Breadcrumb;
use App\Components\Cores\Notification;
use App\Components\Menus\Toprightmenu;
use App\Components\Components\FormGroup;


/**
 * Command
 */
use App\View\Components\Datatables;



class AdminServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {

        /**
         * Pagination
         */
        Paginator::useBootstrap();

        /**
         * definde gates
         */
        $menu = new Menu;
        $gates = $menu->gates($menu->menus);
        if(is_array($gates)) {
            foreach($gates as $gate) {
                Gate::define($gate, function(Authenticatable $user) use ($gate) {
                    foreach($user->roles as $role) {
                        // dd($role);
                        return in_array($gate, $role->gates);
                    }
                });
            }
        }
    }
}
