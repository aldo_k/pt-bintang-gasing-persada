<?php

namespace App\Providers;


/**
 * Components
 */
use App\View\Cores\Alert;
use App\View\Cores\Layout;
use App\View\Menus\Sidebar;
use App\View\Components\Card;
use App\View\Components\Input;

use App\View\Cores\Breadcrumb;
use App\View\Cores\Notification;

use App\View\Menus\Toprightmenu;

use App\View\Components\FormGroup;
use App\View\Components\Datatables;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Load view template
         */
        $this->loadViewsFrom(resource_path('views') . '/template', 'template');

        /**
         * View Component
         */
        $this->loadViewComponentsAs('template', [
            Card::class,
            Input::class,
            Sidebar::class,
            Breadcrumb::class,
            Toprightmenu::class,
            Datatables::class,
            Alert::class,
            Notification::class,
            FormGroup::class,
            Layout::class
        ]);
    }
}
