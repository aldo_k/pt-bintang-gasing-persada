-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 29, 2021 at 09:49 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pt_bgp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_logables`
--

CREATE TABLE `admin_logables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `logable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logable_id` bigint(20) UNSIGNED NOT NULL,
  `new_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_logables`
--

INSERT INTO `admin_logables` (`id`, `user_id`, `logable_type`, `logable_id`, `new_data`, `old_data`, `type`, `state`, `created_at`, `updated_at`) VALUES
(1, 1, 'App\\Models\\Buyer', 1, '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":\"35\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-23T18:24:55.000000Z\"}', '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":34,\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-23T08:09:56.000000Z\"}', 'edit', NULL, '2021-11-23 18:24:55', '2021-11-23 18:24:55'),
(2, 1, 'App\\Models\\Buyer', 1, '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":\"34\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-23T18:24:57.000000Z\"}', '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":35,\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-23T18:24:55.000000Z\"}', 'edit', NULL, '2021-11-23 18:24:57', '2021-11-23 18:24:57'),
(3, 1, 'App\\Models\\Po', 1, '{\"id\":1,\"po_a\":\"35\",\"po_a_temp\":34,\"po_b\":\"35\",\"po_b_temp\":35,\"target\":\"34\",\"report_time\":\"2021-11-23 20:00:00\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T13:52:59.000000Z\",\"updated_at\":\"2021-11-23T18:25:26.000000Z\"}', '{\"id\":1,\"po_a\":34,\"po_a_temp\":34,\"po_b\":35,\"po_b_temp\":35,\"target\":34,\"report_time\":\"2021-11-23 20:00:00\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T13:52:59.000000Z\",\"updated_at\":\"2021-11-23T16:06:33.000000Z\"}', 'edit', NULL, '2021-11-23 18:25:26', '2021-11-23 18:25:26'),
(4, 1, 'App\\Models\\Po', 5, '{\"target\":\"34\",\"po_a\":\"34\",\"po_b\":\"36\",\"po_a_temp\":\"34\",\"po_b_temp\":\"36\",\"report_time\":\"2021-11-24 13:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-24T06:49:48.000000Z\",\"created_at\":\"2021-11-24T06:49:48.000000Z\",\"id\":5}', '[]', 'create', NULL, '2021-11-24 06:49:48', '2021-11-24 06:49:48'),
(5, 1, 'App\\Models\\Po', 9, '{\"target\":\"34\",\"po_a\":\"36\",\"po_b\":\"36\",\"po_a_temp\":\"36\",\"po_b_temp\":\"36\",\"report_time\":\"2021-11-24 13:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-24T06:59:37.000000Z\",\"created_at\":\"2021-11-24T06:59:37.000000Z\",\"id\":9}', '[]', 'create', NULL, '2021-11-24 06:59:37', '2021-11-24 06:59:37'),
(6, 1, 'App\\Models\\Po', 50, '{\"target\":\"34\",\"po_a\":\"34\",\"po_b\":\"34\",\"po_a_temp\":\"34\",\"po_b_temp\":\"34\",\"report_time\":\"2021-11-25 22:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-25T15:42:57.000000Z\",\"created_at\":\"2021-11-25T15:42:57.000000Z\",\"id\":50}', '[]', 'create', NULL, '2021-11-25 15:42:57', '2021-11-25 15:42:57'),
(7, 1, 'App\\Models\\Po', 51, '{\"target\":\"34\",\"po_a\":\"36\",\"po_b\":\"34\",\"po_a_temp\":\"36\",\"po_b_temp\":\"34\",\"report_time\":\"2021-11-26 01:00:00\",\"created_by\":1,\"updated_at\":\"2021-11-25T18:00:56.000000Z\",\"created_at\":\"2021-11-25T18:00:56.000000Z\",\"id\":51}', '[]', 'create', NULL, '2021-11-25 18:00:56', '2021-11-25 18:00:56'),
(8, 1, 'App\\Models\\Po', 52, '{\"target\":\"34\",\"po_a\":\"45\",\"po_b\":\"45\",\"po_a_temp\":\"45\",\"po_b_temp\":\"45\",\"report_time\":\"2021-11-26 01:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-25T18:33:54.000000Z\",\"created_at\":\"2021-11-25T18:33:54.000000Z\",\"id\":52}', '[]', 'create', NULL, '2021-11-25 18:33:54', '2021-11-25 18:33:54'),
(9, 1, 'App\\Models\\Po', 53, '{\"target\":\"34\",\"po_a\":\"44\",\"po_b\":\"44\",\"po_a_temp\":\"44\",\"po_b_temp\":\"44\",\"report_time\":\"2021-11-26 21:00:00\",\"created_by\":1,\"updated_at\":\"2021-11-26T14:06:26.000000Z\",\"created_at\":\"2021-11-26T14:06:26.000000Z\",\"id\":53}', '[]', 'create', NULL, '2021-11-26 14:06:26', '2021-11-26 14:06:26'),
(10, 1, 'App\\Models\\Po', 54, '{\"target\":\"34\",\"po_a\":\"56\",\"po_b\":\"56\",\"po_a_temp\":\"56\",\"po_b_temp\":\"56\",\"report_time\":\"2021-11-27 00:00:00\",\"created_by\":1,\"updated_at\":\"2021-11-26T17:21:19.000000Z\",\"created_at\":\"2021-11-26T17:21:19.000000Z\",\"id\":54}', '[]', 'create', NULL, '2021-11-26 17:21:19', '2021-11-26 17:21:19'),
(11, 1, 'App\\Models\\Role', 1, '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.pofixed.index\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-26T17:23:28.000000Z\"}', '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-23T07:29:45.000000Z\"}', 'edit', NULL, '2021-11-26 17:23:28', '2021-11-26 17:23:28'),
(12, 1, 'App\\Models\\Role', 1, '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.po-fixed.index\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-26T17:24:25.000000Z\"}', '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.pofixed.index\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-26T17:23:28.000000Z\"}', 'edit', NULL, '2021-11-26 17:24:25', '2021-11-26 17:24:25'),
(13, 1, 'App\\Models\\Role', 1, '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.fixed-po.index\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-26T17:25:59.000000Z\"}', '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.po-fixed.index\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-26T17:24:25.000000Z\"}', 'edit', NULL, '2021-11-26 17:25:59', '2021-11-26 17:25:59'),
(14, 1, 'App\\Models\\Po', 55, '{\"target\":\"34\",\"po_a\":\"45\",\"po_b\":\"45\",\"po_a_temp\":\"45\",\"po_b_temp\":\"45\",\"report_time\":\"2021-11-27 00:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-26T17:36:30.000000Z\",\"created_at\":\"2021-11-26T17:36:30.000000Z\",\"id\":55}', '[]', 'create', NULL, '2021-11-26 17:36:30', '2021-11-26 17:36:30'),
(15, 1, 'App\\Models\\Po', 56, '{\"target\":\"34\",\"po_a\":\"34\",\"po_b\":\"34\",\"po_a_temp\":\"34\",\"po_b_temp\":\"34\",\"report_time\":\"2021-11-28 22:00:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T15:25:42.000000Z\",\"created_at\":\"2021-11-28T15:25:42.000000Z\",\"id\":56}', '[]', 'create', NULL, '2021-11-28 15:25:42', '2021-11-28 15:25:42'),
(16, 1, 'App\\Models\\Po', 57, '{\"target\":\"34\",\"po_a\":\"37\",\"po_b\":\"37\",\"po_a_temp\":\"37\",\"po_b_temp\":\"37\",\"report_time\":\"2021-11-28 22:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T15:40:22.000000Z\",\"created_at\":\"2021-11-28T15:40:22.000000Z\",\"id\":57}', '[]', 'create', NULL, '2021-11-28 15:40:22', '2021-11-28 15:40:22'),
(17, 1, 'App\\Models\\Po', 58, '{\"target\":\"34\",\"po_a\":\"37\",\"po_b\":\"31\",\"po_a_temp\":\"37\",\"po_b_temp\":\"31\",\"report_time\":\"2021-11-28 23:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T16:46:34.000000Z\",\"created_at\":\"2021-11-28T16:46:34.000000Z\",\"id\":58}', '[]', 'create', NULL, '2021-11-28 16:46:34', '2021-11-28 16:46:34'),
(18, 1, 'App\\Models\\Buyer', 1, '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"31\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-28T17:02:29.000000Z\"}', '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":34,\"usl\":36,\"lsl\":33,\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-28T16:57:32.000000Z\"}', 'edit', NULL, '2021-11-28 17:02:29', '2021-11-28 17:02:29'),
(19, 1, 'App\\Models\\Buyer', 1, '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-28T17:02:32.000000Z\"}', '{\"id\":1,\"name\":\"SMPT, YOKOHAMA\",\"target\":34,\"usl\":36,\"lsl\":31,\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T14:49:13.000000Z\",\"updated_at\":\"2021-11-28T17:02:29.000000Z\"}', 'edit', NULL, '2021-11-28 17:02:32', '2021-11-28 17:02:32'),
(20, 1, 'App\\Models\\Po', 1, '{\"id\":1,\"po_a\":\"35\",\"po_a_temp\":34,\"po_b\":\"35\",\"po_b_temp\":35,\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"report_time\":\"2021-11-23 20:00:00\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T13:52:59.000000Z\",\"updated_at\":\"2021-11-28T17:14:53.000000Z\"}', '{\"id\":1,\"po_a\":35,\"po_a_temp\":34,\"po_b\":35,\"po_b_temp\":35,\"target\":34,\"usl\":null,\"lsl\":null,\"report_time\":\"2021-11-23 20:00:00\",\"created_by\":1,\"updated_by\":1,\"created_at\":\"2021-11-23T13:52:59.000000Z\",\"updated_at\":\"2021-11-23T18:25:26.000000Z\"}', 'edit', NULL, '2021-11-28 17:14:53', '2021-11-28 17:14:53'),
(21, 1, 'App\\Models\\Role', 1, '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.fixed-po.index\",\"administrator.daily-po.index\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-28T17:44:33.000000Z\"}', '{\"id\":1,\"name\":\"IT Development\",\"gates\":[\"administrator.chart.index\",\"administrator.buyer.index\",\"administrator.buyer.create\",\"administrator.buyer.update\",\"administrator.buyer.destroy\",\"administrator.po.index\",\"administrator.po.create\",\"administrator.po.update\",\"administrator.po.destroy\",\"administrator.fixed-po.index\",\"administrator.account\",\"administrator.account.admin.index\",\"administrator.account.admin.create\",\"administrator.account.admin.update\",\"administrator.account.admin.destroy\",\"administrator.access\",\"administrator.access.role.index\",\"administrator.access.role.create\",\"administrator.access.role.update\",\"administrator.access.role.destroy\",\"administrator.access.permission.index\",\"administrator.access.permission.show\",\"administrator.access.permission.assign\",\"administrator.system\",\"administrator.system.activity.index\",\"administrator.system.activity.delete\",\"administrator.system.log.index\",\"administrator.profile.index\",\"administrator.profile.update\"],\"created_at\":\"2021-11-23T05:09:21.000000Z\",\"updated_at\":\"2021-11-26T17:25:59.000000Z\"}', 'edit', NULL, '2021-11-28 17:44:33', '2021-11-28 17:44:33'),
(22, 1, 'App\\Models\\Po', 59, '{\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"po_a\":\"34\",\"po_b\":\"34\",\"po_a_temp\":\"34\",\"po_b_temp\":\"34\",\"report_time\":\"2021-11-29 00:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T17:55:54.000000Z\",\"created_at\":\"2021-11-28T17:55:54.000000Z\",\"id\":59}', '[]', 'create', NULL, '2021-11-28 17:55:54', '2021-11-28 17:55:54'),
(23, 1, 'App\\Models\\Po', 60, '{\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"po_a\":\"34\",\"po_b\":\"34\",\"po_a_temp\":\"34\",\"po_b_temp\":\"34\",\"report_time\":\"2021-11-29 01:00:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T18:15:37.000000Z\",\"created_at\":\"2021-11-28T18:15:37.000000Z\",\"id\":60}', '[]', 'create', NULL, '2021-11-28 18:15:37', '2021-11-28 18:15:37'),
(24, 1, 'App\\Models\\Po', 61, '{\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"po_a\":\"35\",\"po_b\":\"35\",\"po_a_temp\":\"35\",\"po_b_temp\":\"35\",\"report_time\":\"2021-11-29 01:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T18:35:58.000000Z\",\"created_at\":\"2021-11-28T18:35:58.000000Z\",\"id\":61}', '[]', 'create', NULL, '2021-11-28 18:35:58', '2021-11-28 18:35:58'),
(25, 1, 'App\\Models\\Po', 62, '{\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"po_a\":\"36\",\"po_b\":\"38\",\"po_a_temp\":\"36\",\"po_b_temp\":\"38\",\"report_time\":\"2021-11-29 02:00:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T19:01:41.000000Z\",\"created_at\":\"2021-11-28T19:01:41.000000Z\",\"id\":62}', '[]', 'create', NULL, '2021-11-28 19:01:41', '2021-11-28 19:01:41'),
(26, 1, 'App\\Models\\DailyReport', 1, '{\"date_report\":\"2021-11-29\",\"block_a\":null,\"block_b\":null,\"drc_a\":null,\"drc_b\":null,\"po_a\":null,\"po_b\":null,\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"updated_at\":\"2021-11-28T19:30:52.000000Z\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"id\":1}', '[]', 'create', NULL, '2021-11-28 19:30:52', '2021-11-28 19:30:52'),
(27, 1, 'App\\Models\\DailyReport', 1, '{\"id\":1,\"block_a\":\"Bismillah\",\"block_b\":null,\"drc_a\":null,\"drc_b\":null,\"po_a\":null,\"po_b\":null,\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:31:07.000000Z\",\"status\":null}', '{\"id\":1,\"block_a\":null,\"block_b\":null,\"drc_a\":null,\"drc_b\":null,\"po_a\":null,\"po_b\":null,\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:30:52.000000Z\",\"status\":null}', 'edit', NULL, '2021-11-28 19:31:07', '2021-11-28 19:31:07'),
(28, 1, 'App\\Models\\DailyReport', 1, '{\"id\":1,\"block_a\":\"Bismillah\",\"block_b\":null,\"drc_a\":\"Ganti\",\"drc_b\":null,\"po_a\":null,\"po_b\":null,\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:37:16.000000Z\",\"status\":null}', '{\"id\":1,\"block_a\":\"Bismillah\",\"block_b\":null,\"drc_a\":null,\"drc_b\":null,\"po_a\":null,\"po_b\":null,\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:31:07.000000Z\",\"status\":null}', 'edit', NULL, '2021-11-28 19:37:16', '2021-11-28 19:37:16'),
(29, 1, 'App\\Models\\DailyReport', 1, '{\"id\":1,\"block_a\":\"Bismillah\",\"block_b\":null,\"drc_a\":\"Ganti\",\"drc_b\":null,\"po_a\":null,\"po_b\":\"Coba\",\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:37:20.000000Z\",\"status\":null}', '{\"id\":1,\"block_a\":\"Bismillah\",\"block_b\":null,\"drc_a\":\"Ganti\",\"drc_b\":null,\"po_a\":null,\"po_b\":null,\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:37:16.000000Z\",\"status\":null}', 'edit', NULL, '2021-11-28 19:37:20', '2021-11-28 19:37:20'),
(30, 1, 'App\\Models\\DailyReport', 1, '{\"id\":1,\"block_a\":\"R10+R12\",\"block_b\":\"R10+R12\",\"drc_a\":\"86%\",\"drc_b\":\"86%\",\"po_a\":\"37\",\"po_b\":\"38\",\"pri_a\":\"72\",\"pri_b\":\"72\",\"umur_jemur_a\":\"13 Hari\",\"umur_jemur_b\":\"13 Hari\",\"kamar_a\":\"9B\",\"kamar_b\":\"8B\",\"target_a\":\"33 (31-34)\",\"target_b\":\"33 (31-43)\",\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:38:51.000000Z\",\"status\":null}', '{\"id\":1,\"block_a\":\"Bismillah\",\"block_b\":null,\"drc_a\":\"Ganti\",\"drc_b\":null,\"po_a\":null,\"po_b\":\"Coba\",\"pri_a\":null,\"pri_b\":null,\"umur_jemur_a\":null,\"umur_jemur_b\":null,\"kamar_a\":null,\"kamar_b\":null,\"target_a\":null,\"target_b\":null,\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:37:20.000000Z\",\"status\":null}', 'edit', NULL, '2021-11-28 19:38:51', '2021-11-28 19:38:51'),
(31, 1, 'App\\Models\\Po', 63, '{\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"po_a\":\"45\",\"po_b\":\"45\",\"po_a_temp\":\"45\",\"po_b_temp\":\"45\",\"report_time\":\"2021-11-29 02:30:00\",\"created_by\":1,\"updated_at\":\"2021-11-28T19:41:07.000000Z\",\"created_at\":\"2021-11-28T19:41:07.000000Z\",\"id\":63}', '[]', 'create', NULL, '2021-11-28 19:41:07', '2021-11-28 19:41:07'),
(32, 1, 'App\\Models\\Po', 64, '{\"target\":\"34\",\"usl\":\"36\",\"lsl\":\"32\",\"po_a\":\"36\",\"po_b\":\"32\",\"po_a_temp\":\"36\",\"po_b_temp\":\"32\",\"report_time\":\"2021-11-29 15:00:00\",\"created_by\":1,\"updated_at\":\"2021-11-29T08:08:55.000000Z\",\"created_at\":\"2021-11-29T08:08:55.000000Z\",\"id\":64}', '[]', 'create', NULL, '2021-11-29 08:08:55', '2021-11-29 08:08:55'),
(33, 1, 'App\\Models\\DailyReport', 1, '{\"id\":1,\"cooper_a\":\"COOPER\",\"cooper_b\":\"COOPER\",\"block_a\":\"R10+R12\",\"block_b\":\"R10+R12\",\"drc_a\":\"86%\",\"drc_b\":\"86%\",\"po_a\":\"37\",\"po_b\":\"38\",\"pri_a\":\"72\",\"pri_b\":\"72\",\"umur_jemur_a\":\"13 Hari\",\"umur_jemur_b\":\"13 Hari\",\"kamar_a\":\"9B\",\"kamar_b\":\"8B\",\"target_a\":\"33 (31-34)\",\"target_b\":\"33 (31-43)\",\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-29T09:46:57.000000Z\",\"status\":null}', '{\"id\":1,\"cooper_a\":null,\"cooper_b\":null,\"block_a\":\"R10+R12\",\"block_b\":\"R10+R12\",\"drc_a\":\"86%\",\"drc_b\":\"86%\",\"po_a\":\"37\",\"po_b\":\"38\",\"pri_a\":\"72\",\"pri_b\":\"72\",\"umur_jemur_a\":\"13 Hari\",\"umur_jemur_b\":\"13 Hari\",\"kamar_a\":\"9B\",\"kamar_b\":\"8B\",\"target_a\":\"33 (31-34)\",\"target_b\":\"33 (31-43)\",\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-28T19:38:51.000000Z\",\"status\":null}', 'edit', NULL, '2021-11-29 09:46:57', '2021-11-29 09:46:57'),
(34, 1, 'App\\Models\\DailyReport', 1, '{\"id\":1,\"cooper_a\":\"COOPER A\",\"cooper_b\":\"COOPER\",\"block_a\":\"R10+R12\",\"block_b\":\"R10+R12\",\"drc_a\":\"86%\",\"drc_b\":\"86%\",\"po_a\":\"37\",\"po_b\":\"38\",\"pri_a\":\"72\",\"pri_b\":\"72\",\"umur_jemur_a\":\"13 Hari\",\"umur_jemur_b\":\"13 Hari\",\"kamar_a\":\"9B\",\"kamar_b\":\"8B\",\"target_a\":\"33 (31-34)\",\"target_b\":\"33 (31-43)\",\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-29T09:47:11.000000Z\",\"status\":null}', '{\"id\":1,\"cooper_a\":\"COOPER\",\"cooper_b\":\"COOPER\",\"block_a\":\"R10+R12\",\"block_b\":\"R10+R12\",\"drc_a\":\"86%\",\"drc_b\":\"86%\",\"po_a\":\"37\",\"po_b\":\"38\",\"pri_a\":\"72\",\"pri_b\":\"72\",\"umur_jemur_a\":\"13 Hari\",\"umur_jemur_b\":\"13 Hari\",\"kamar_a\":\"9B\",\"kamar_b\":\"8B\",\"target_a\":\"33 (31-34)\",\"target_b\":\"33 (31-43)\",\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-29T09:46:57.000000Z\",\"status\":null}', 'edit', NULL, '2021-11-29 09:47:11', '2021-11-29 09:47:11'),
(35, 1, 'App\\Models\\DailyReport', 1, '{\"id\":1,\"cooper_a\":\"COOPER\",\"cooper_b\":\"COOPER\",\"block_a\":\"R10+R12\",\"block_b\":\"R10+R12\",\"drc_a\":\"86%\",\"drc_b\":\"86%\",\"po_a\":\"37\",\"po_b\":\"38\",\"pri_a\":\"72\",\"pri_b\":\"72\",\"umur_jemur_a\":\"13 Hari\",\"umur_jemur_b\":\"13 Hari\",\"kamar_a\":\"9B\",\"kamar_b\":\"8B\",\"target_a\":\"33 (31-34)\",\"target_b\":\"33 (31-43)\",\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-29T09:47:15.000000Z\",\"status\":null}', '{\"id\":1,\"cooper_a\":\"COOPER A\",\"cooper_b\":\"COOPER\",\"block_a\":\"R10+R12\",\"block_b\":\"R10+R12\",\"drc_a\":\"86%\",\"drc_b\":\"86%\",\"po_a\":\"37\",\"po_b\":\"38\",\"pri_a\":\"72\",\"pri_b\":\"72\",\"umur_jemur_a\":\"13 Hari\",\"umur_jemur_b\":\"13 Hari\",\"kamar_a\":\"9B\",\"kamar_b\":\"8B\",\"target_a\":\"33 (31-34)\",\"target_b\":\"33 (31-43)\",\"date_report\":\"2021-11-29\",\"created_at\":\"2021-11-28T19:30:52.000000Z\",\"updated_at\":\"2021-11-29T09:47:11.000000Z\",\"status\":null}', 'edit', NULL, '2021-11-29 09:47:15', '2021-11-29 09:47:15');

-- --------------------------------------------------------

--
-- Table structure for table `admin_options`
--

CREATE TABLE `admin_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gates` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `gates`, `created_at`, `updated_at`) VALUES
(1, 'IT Development', '[\"administrator.chart.index\", \"administrator.buyer.index\", \"administrator.buyer.create\", \"administrator.buyer.update\", \"administrator.buyer.destroy\", \"administrator.po.index\", \"administrator.po.create\", \"administrator.po.update\", \"administrator.po.destroy\", \"administrator.fixed-po.index\", \"administrator.daily-po.index\", \"administrator.account\", \"administrator.account.admin.index\", \"administrator.account.admin.create\", \"administrator.account.admin.update\", \"administrator.account.admin.destroy\", \"administrator.access\", \"administrator.access.role.index\", \"administrator.access.role.create\", \"administrator.access.role.update\", \"administrator.access.role.destroy\", \"administrator.access.permission.index\", \"administrator.access.permission.show\", \"administrator.access.permission.assign\", \"administrator.system\", \"administrator.system.activity.index\", \"administrator.system.activity.delete\", \"administrator.system.log.index\", \"administrator.profile.index\", \"administrator.profile.update\"]', '2021-11-23 05:09:21', '2021-11-28 17:44:33');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_user`
--

CREATE TABLE `admin_role_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_user`
--

INSERT INTO `admin_role_user` (`role_id`, `user_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `buyers`
--

CREATE TABLE `buyers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` tinyint(2) NOT NULL DEFAULT '0',
  `usl` tinyint(2) DEFAULT '0',
  `lsl` tinyint(2) DEFAULT '0',
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buyers`
--

INSERT INTO `buyers` (`id`, `name`, `target`, `usl`, `lsl`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'SMPT, YOKOHAMA', 34, 36, 32, 1, 1, '2021-11-23 14:49:13', '2021-11-28 17:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `daily_report`
--

CREATE TABLE `daily_report` (
  `id` bigint(20) NOT NULL,
  `cooper_a` varchar(191) DEFAULT NULL,
  `cooper_b` varchar(191) DEFAULT NULL,
  `block_a` varchar(191) DEFAULT NULL,
  `block_b` varchar(191) DEFAULT NULL,
  `drc_a` varchar(191) DEFAULT NULL,
  `drc_b` varchar(191) DEFAULT NULL,
  `po_a` varchar(191) DEFAULT NULL,
  `po_b` varchar(191) DEFAULT NULL,
  `pri_a` varchar(191) DEFAULT NULL,
  `pri_b` varchar(191) DEFAULT NULL,
  `umur_jemur_a` varchar(191) DEFAULT NULL,
  `umur_jemur_b` varchar(191) DEFAULT NULL,
  `kamar_a` varchar(191) DEFAULT NULL,
  `kamar_b` varchar(191) DEFAULT NULL,
  `target_a` varchar(191) DEFAULT NULL,
  `target_b` varchar(191) DEFAULT NULL,
  `date_report` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_report`
--

INSERT INTO `daily_report` (`id`, `cooper_a`, `cooper_b`, `block_a`, `block_b`, `drc_a`, `drc_b`, `po_a`, `po_b`, `pri_a`, `pri_b`, `umur_jemur_a`, `umur_jemur_b`, `kamar_a`, `kamar_b`, `target_a`, `target_b`, `date_report`, `created_at`, `updated_at`, `status`) VALUES
(1, 'COOPER', 'COOPER', 'R10+R12', 'R10+R12', '86%', '86%', '37', '38', '72', '72', '13 Hari', '13 Hari', '9B', '8B', '33 (31-34)', '33 (31-43)', '2021-11-29', '2021-11-28 19:30:52', '2021-11-29 09:47:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '0000_00_00_000000_create_admin_logables_table', 1),
(2, '0000_00_00_000000_create_admin_options_table', 1),
(3, '0000_00_00_000000_create_role_user_table', 1),
(4, '0000_00_00_000000_create_roles_table', 1),
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(9, '2021_09_10_150147_create_notifications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE `po` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_a` tinyint(2) NOT NULL DEFAULT '0',
  `po_a_temp` tinyint(2) NOT NULL DEFAULT '0',
  `po_b` tinyint(2) NOT NULL DEFAULT '0',
  `po_b_temp` tinyint(2) NOT NULL DEFAULT '0',
  `target` tinyint(2) NOT NULL DEFAULT '0',
  `usl` tinyint(2) NOT NULL DEFAULT '0',
  `lsl` tinyint(2) NOT NULL DEFAULT '0',
  `report_time` datetime DEFAULT NULL,
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `po`
--

INSERT INTO `po` (`id`, `po_a`, `po_a_temp`, `po_b`, `po_b_temp`, `target`, `usl`, `lsl`, `report_time`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 35, 34, 35, 35, 34, 37, 32, '2021-11-23 20:00:00', 1, 1, '2021-11-23 13:52:59', '2021-11-28 17:24:24'),
(2, 35, 35, 36, 36, 34, 37, 32, '2021-11-23 20:30:00', 1, 1, '2021-11-23 13:52:59', '2021-11-28 17:24:31'),
(3, 36, 36, 34, 34, 34, 37, 32, '2021-11-23 21:00:00', 1, 1, '2021-11-23 15:53:35', '2021-11-28 17:24:41'),
(4, 36, 34, 36, 35, 34, 37, 32, '2021-11-23 21:30:00', 1, 1, '2021-11-23 16:01:15', '2021-11-28 17:24:41'),
(5, 34, 34, 36, 36, 34, 37, 32, '2021-11-23 22:00:00', 1, NULL, '2021-11-24 06:49:48', '2021-11-28 17:24:41'),
(6, 35, 35, 37, 37, 34, 37, 32, '2021-11-23 22:30:00', 1, NULL, '2021-11-24 06:49:48', '2021-11-28 17:24:41'),
(7, 35, 35, 37, 37, 34, 37, 32, '2021-11-23 23:00:00', 1, NULL, '2021-11-24 06:49:48', '2021-11-28 17:24:41'),
(8, 35, 35, 37, 37, 34, 37, 32, '2021-11-23 23:30:00', 1, NULL, '2021-11-24 06:49:48', '2021-11-28 17:24:41'),
(9, 35, 33, 32, 33, 34, 37, 32, '2021-11-24 00:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(10, 31, 33, 34, 36, 34, 37, 32, '2021-11-24 00:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(11, 32, 34, 35, 33, 34, 37, 32, '2021-11-24 01:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(12, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 01:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(13, 34, 34, 33, 33, 34, 37, 32, '2021-11-24 02:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(14, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 02:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(15, 31, 32, 32, 31, 34, 37, 32, '2021-11-24 03:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(16, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 03:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(17, 34, 34, 33, 33, 34, 37, 32, '2021-11-24 04:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(18, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 04:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(19, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 05:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(20, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 05:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(21, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 06:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(22, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 06:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(23, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 07:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(24, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 07:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(25, 40, 40, 40, 40, 34, 37, 32, '2021-11-24 08:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(26, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 08:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(27, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 09:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(28, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 09:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(29, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 10:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(30, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 10:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(31, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 11:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(32, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 11:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(33, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 12:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(34, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 12:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(35, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 13:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(36, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 13:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(37, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 14:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(38, 31, 31, 31, 31, 34, 37, 32, '2021-11-24 14:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(39, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 15:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(40, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 15:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(41, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 16:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(42, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 16:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(43, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 17:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(44, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 17:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(45, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 18:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(46, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 18:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(47, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 19:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(48, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 19:30:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(49, 36, 36, 36, 36, 34, 37, 32, '2021-11-24 20:00:00', 1, NULL, '2021-11-24 06:59:37', '2021-11-28 17:24:41'),
(50, 34, 34, 34, 34, 34, 37, 32, '2021-11-25 22:30:00', 1, NULL, '2021-11-25 15:42:57', '2021-11-28 17:24:41'),
(51, 40, 40, 40, 40, 34, 37, 32, '2021-11-26 01:00:00', 1, NULL, '2021-11-25 18:00:56', '2021-11-28 17:24:41'),
(52, 45, 45, 45, 45, 34, 37, 32, '2021-11-26 01:30:00', 1, NULL, '2021-11-25 18:33:54', '2021-11-28 17:24:41'),
(53, 44, 44, 44, 44, 34, 37, 32, '2021-11-26 21:00:00', 1, NULL, '2021-11-26 14:06:26', '2021-11-28 17:24:41'),
(54, 56, 56, 56, 56, 34, 37, 32, '2021-11-27 00:00:00', 1, NULL, '2021-11-26 17:21:19', '2021-11-28 17:24:41'),
(55, 45, 45, 45, 45, 34, 37, 32, '2021-11-27 00:30:00', 1, NULL, '2021-11-26 17:36:30', '2021-11-28 17:24:41'),
(56, 36, 36, 36, 36, 34, 37, 32, '2021-11-28 22:00:00', 1, NULL, '2021-11-28 15:25:42', '2021-11-28 17:24:41'),
(57, 33, 33, 33, 33, 34, 37, 32, '2021-11-28 22:30:00', 1, NULL, '2021-11-28 15:40:22', '2021-11-28 17:24:41'),
(58, 37, 37, 31, 31, 34, 37, 32, '2021-11-28 23:30:00', 1, NULL, '2021-11-28 16:46:34', '2021-11-28 17:24:41'),
(59, 34, 34, 34, 34, 34, 36, 32, '2021-11-29 00:30:00', 1, NULL, '2021-11-28 17:55:54', '2021-11-28 17:55:54'),
(60, 34, 34, 34, 34, 34, 36, 32, '2021-11-29 01:00:00', 1, NULL, '2021-11-28 18:15:37', '2021-11-28 18:15:37'),
(61, 45, 45, 35, 35, 34, 36, 32, '2021-11-29 01:30:00', 1, NULL, '2021-11-28 18:35:58', '2021-11-28 18:42:00'),
(62, 36, 36, 38, 38, 34, 36, 32, '2021-11-29 02:00:00', 1, NULL, '2021-11-28 19:01:41', '2021-11-28 19:01:41'),
(63, 45, 45, 45, 45, 34, 36, 32, '2021-11-29 02:30:00', 1, NULL, '2021-11-28 19:41:07', '2021-11-28 19:41:07'),
(64, 36, 36, 32, 32, 34, 36, 32, '2021-11-29 15:00:00', 1, NULL, '2021-11-29 08:08:55', '2021-11-29 08:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Oswald Champlin', 'kaitlin.west@example.org', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NBzJzlhSH4Y1iHi2pGgWAmXSBjgDXMU3lMoRf3e3CdQ4VQsWZ7d09Jiy9cDj', '2021-11-23 05:09:22', '2021-11-28 17:35:46'),
(2, 'Dr. Philip Macejkovic', 'fay.jeff@example.net', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6k6boofGEP', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(3, 'Drake Batz III', 'kaci89@example.net', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6mpn45sXc3', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(4, 'Mr. Jordon Sipes PhD', 'hoppe.emmy@example.org', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'V26hiQbgn8', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(5, 'Melyssa Muller', 'xdoyle@example.net', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'vGRoGJTVHL', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(6, 'Owen Rowe', 'hand.otho@example.org', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UQyJUbiZeK', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(7, 'Skyla Eichmann', 'nmoore@example.org', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'vEZPpxQd6P', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(8, 'Rosa Marks', 'cristina98@example.com', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'v7nXLa3PFR', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(9, 'Evelyn Wolff MD', 'quincy.mann@example.net', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bJcMArS3AH', '2021-11-23 05:09:22', '2021-11-23 05:09:22'),
(10, 'Miss Alia Weissnat V', 'cristopher.shields@example.net', '2021-11-23 05:09:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'g6H2GEPSPy', '2021-11-23 05:09:22', '2021-11-23 05:09:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_logables`
--
ALTER TABLE `admin_logables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_logables_logable_type_logable_id_index` (`logable_type`,`logable_id`);

--
-- Indexes for table `admin_options`
--
ALTER TABLE `admin_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_options_option_name_unique` (`option_name`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buyers`
--
ALTER TABLE `buyers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_report`
--
ALTER TABLE `daily_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `po`
--
ALTER TABLE `po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_logables`
--
ALTER TABLE `admin_logables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `admin_options`
--
ALTER TABLE `admin_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buyers`
--
ALTER TABLE `buyers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `daily_report`
--
ALTER TABLE `daily_report`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `po`
--
ALTER TABLE `po`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
