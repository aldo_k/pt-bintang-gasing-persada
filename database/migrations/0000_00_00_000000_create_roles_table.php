<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\Menu;
use App\Models\Role;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('admin_roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->json('gates')->nullable();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();

        // Insert for first data
        $menu = new Menu;
        Role::create([
            'name' => 'Super Admin',
            'gates' => $menu->gates($menu->menus) ?? []
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('admin_roles');
        Schema::enableForeignKeyConstraints();
    }
}
