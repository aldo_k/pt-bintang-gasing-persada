google.charts.load('current', {
    'packages': ['corechart', 'line']
});

function renderChart() {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    $.getJSON(`/administrator/chart/data/${date}`, function (json) {

        let html_thead0 = `<tr><th>Hour</th>`;
        let html_tbody1 = '<tr><td>Po (A)</td>';
        let html_tbody2 = '<tr><td>Po (B)</td>';
        let html_tbody3 = '<tr><td>Target</td>';

        let len_data = json.chart_data;
        $.each(json.chart_data, (index, data) => {

            if (index == len_data)
                return false;

            let hour = data[0].split(":").join("<br>:<br>").replace(':', '....');

            html_thead0 += `<th>${hour}</th>`;
            html_tbody1 += `<td>${data[1]==null ? '-' : data[1]}</td>`;
            html_tbody2 += `<td>${data[2]==null ? '-' : data[2]}</td>`;
            html_tbody3 += `<td>${data[1]==null ? '-' : data[3]}</td>`;

        });

        html_thead0 += `</tr>`;
        html_tbody1 += `</tr>`;
        html_tbody2 += `</tr>`;
        html_tbody3 += `</tr>`;

        $('#render-thead').html(html_thead0);
        $('#render-tbody').html(html_tbody1 + html_tbody2 + html_tbody3);
        
        html_thead0 = `<tr><th>Hour</th>`;
        html_tbody1 = '<tr><td>Po (A)</td>';
        html_tbody2 = '<tr><td>Po (B)</td>';
        html_tbody3 = '<tr><td>Target</td>';

        let len_data_temp = json.chart_data_temp;
        $.each(json.chart_data_temp, (index, data) => {

            if (index == len_data_temp)
                return false;

            let hour = data[0].split(":").join("<br>:<br>").replace(':', '....');

            html_thead0 += `<th>${hour}</th>`;
            html_tbody1 += `<td>${data[1]==null ? '-' : data[1]}</td>`;
            html_tbody2 += `<td>${data[2]==null ? '-' : data[2]}</td>`;
            html_tbody3 += `<td>${data[1]==null ? '-' : data[3]}</td>`;

        });

        html_thead0 += `</tr>`;
        html_tbody1 += `</tr>`;
        html_tbody2 += `</tr>`;
        html_tbody3 += `</tr>`;

        $('#render-thead-temp').html(html_thead0);
        $('#render-tbody-temp').html(html_tbody1 + html_tbody2 + html_tbody3);

        let date = json.date;
        let date_start = date[0].split('T')[0];
        let date_end = date[1].split('T')[0];

        var options = {
            title: `SPC Po, Production - ${date_start} 20:00 until ${date_end} 20:00`,
            fontSize: 14,
            chartArea: {
                height: '100%',
                width: '70%',
                top: 48,
                left: 48,
                right: 114,
                bottom: 48
            },
            width: '100%',
            height: 800,
            axes: {
                x: {
                    0: {
                        side: 'bottom'
                    }
                }
            },
            legend: 
                {
                    'position':'center',
                    'alignment':'center'
                },
            series: {
                0: {
                    color: '#43459d',
                    pointSize: 4
                },
                1: {
                    color: '#6f9654',
                    pointSize: 4
                },
                2: {
                    color: '#f1ca3a',
                    pointSize: 0
                },
                3: {
                    color: '#001100',
                    pointSize: 0
                },
                4: {
                    color: '#001100',
                    pointSize: 0
                },
                5: {
                    color: '#e2431e',
                    pointSize: 0
                },
                6: {
                    color: '#e2431e',
                    pointSize: 0
                },
                7: {
                    color: '#e2431e',
                    pointSize: 0
                }

            },
            vAxis: {
                scaleType: 'log',
                ticks: [...range(json.min_y, json.max_y)],
                viewWindowMode: 'explicit',
                viewWindow: {
                    max: json.max_y,
                    min: json.min_y
                },
                titleTextStyle: {
                    fontSize: 5
                }
            },
            hAxis: {
                slantedText: true,
                slantedTextAngle: 90
            }

        };

        // Fault Tolerance Google Chart Error
        timeout = setInterval(() => {
            if (google.visualization != undefined) {
                google.charts.setOnLoadCallback(drawChartTemp(json, options, date_start, date_end));
                google.charts.setOnLoadCallback(drawChart(json, options, date_start, date_end));
                clearInterval(timeout);
            }
        }, 300);


    });
}
renderChart();

drawChartTemp = (json, options, date_start, date_end) => {

    let data = new google.visualization.DataTable();
    data.addColumn('string', 'Hour');
    data.addColumn('number', 'Po (A)');
    data.addColumn('number', 'Po (B)');
    data.addColumn('number', 'Target');
    data.addColumn('number', 'UCL');
    data.addColumn('number', 'LCL');
    data.addColumn('number', 'USL');
    data.addColumn('number', 'LSL');

    data.addRows(json.chart_data_temp);

    let chart = new google.visualization.LineChart(document.getElementById('po_temp'));

    chart.draw(data, google.charts.Line.convertOptions(options));

    let report_pdf_internal = document.getElementById('save-pdf-internal');
    google.visualization.events.addListener(chart, 'ready', () => {
        report_pdf_internal.disabled = false;
    });

    if (json.current_temp) {
        let current_temp = json.current_temp;

        isPoAOverLimit = current_temp[1] > current_temp[4] || current_temp[1] < current_temp[5];
        isPoBOverLimit = current_temp[2] > current_temp[4] || current_temp[2] < current_temp[5];

        // Set Current Time
        $('#current-time').val(current_temp[0]);

        let time_silent = json.silent_alarm_time;

        let isAlarmIsSetNotSilent = current_temp[0] != time_silent;

        isAlarmIsSetNotSilent && (isPoAOverLimit || isPoBOverLimit) ? startAlarm() : pauseAlarm();

    }

}

drawChart = (json, options, date_start, date_end) => {

    let data = new google.visualization.DataTable();
    data.addColumn('string', 'Hour');
    data.addColumn('number', 'Po (A)');
    data.addColumn('number', 'Po (B)');
    data.addColumn('number', 'Target');
    data.addColumn('number', 'UCL');
    data.addColumn('number', 'LCL');
    data.addColumn('number', 'USL');
    data.addColumn('number', 'LSL');

    data.addRows(json.chart_data);

    let chart = new google.visualization.LineChart(document.getElementById('po'));

    chart.draw(data, google.charts.Line.convertOptions(options));

    let report_pdf_consumer = document.getElementById('save-pdf-consumer');

    google.visualization.events.addListener(chart, 'ready', () => {
        report_pdf_consumer.disabled = false;
    });

}

function* range(start, end) {
    yield start;
    if (start === end) return;
    yield* range(start + 1, end);
}

const activityWatcher = () => {

    let secondsSinceLastActivity = 0;

    let maxInactivity = 10;

    setInterval(() => {
        secondsSinceLastActivity++;
        if (secondsSinceLastActivity >= maxInactivity) {
            (secondsSinceLastActivity % 10) == 0 ? renderChart() : '';

        }
    }, 1000);

    function activity() {
        secondsSinceLastActivity = 0;
    }

    var activityEvents = [
        'mousedown', 'mousemove', 'keydown', 'scroll', 'touchstart'
    ];

    activityEvents.forEach(function (eventName) {
        document.addEventListener(eventName, activity, true);
    });
}

activityWatcher();

var sirineAlarm = document.getElementById("sirine1");

function startAlarm() {

    sirineAlarm.loop = false;
    sirineAlarm.load();
    sirineAlarm.pause();

    sirineAlarm.loop = true;
    sirineAlarm.load();
    sirineAlarm.play();
}

function stopAlarm() {
    pauseAlarm();

    let time_silent = $('#current-time').val();
    if (time_silent) {
        $.getJSON(`/administrator/chart/set-silent-alarm/${time_silent}`, function (data) {
            alert(data.message);
        });
    } else {
        alert('Data Not Exist, Please Add Data Before Silent Alarm!');
    }
}

function pauseAlarm() {
    sirineAlarm.loop = false;
    sirineAlarm.load();
    sirineAlarm.pause();
}
