const App = {
    justDate: (date, region = "id", options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric"
    }) => {
        if (date == null || date == "") {
            return "";
        }

        time = new Date(date).toLocaleTimeString(region, {
            hour: '2-digit',
            minute: '2-digit'
        });
        if (date.length > 10) {
            return new Date(date).toLocaleDateString(region, options);
        }
        return new Date(date).toLocaleDateString(region, options);
    },
}

const getFormInput = (date_current) => {
    $.getJSON(`/administrator/daily-po/find-date/${date_current}`, function (json) {
        $.each(json, function (index, value) { 
             $(`#${index}`).val(value);
        });
    });
}

const renderInfo = () => {
    let today = new Date();
    let date_current = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    $('#date_report').val(date_current);
    $('#current-date').html('PROD ' + App.justDate(date_current));

    getFormInput(date_current);

    $.getJSON(`/administrator/chart/data/${date_current}`, function (json) {

        html_thead0 = `<tr><th>Hour</th>`;
        html_tbody1 = '<tr><td>Po (A)</td>';
        html_tbody2 = '<tr><td>Po (B)</td>';
        html_tbody3 = '<tr><td>Target</td>';

        let len_data_temp = json.chart_data_temp;
        $.each(json.chart_data_temp, (index, data) => {

            if (index == len_data_temp)
                return false;

            let hour = data[0].split(":").join("<br>:<br>").replace(':', '....');

            html_thead0 += `<th>${hour}</th>`;
            html_tbody1 += `<td>${data[1]==null ? '-' : data[1]}</td>`;
            html_tbody2 += `<td>${data[2]==null ? '-' : data[2]}</td>`;
            html_tbody3 += `<td>${data[1]==null ? '-' : data[3]}</td>`;

        });

        html_thead0 += `</tr>`;
        html_tbody1 += `</tr>`;
        html_tbody2 += `</tr>`;
        html_tbody3 += `</tr>`;

        $('#render-thead-temp').html(html_thead0);
        $('#render-tbody-temp').html(html_tbody1 + html_tbody2 + html_tbody3);

        let date = json.date;
        let date_start = date[0].split('T')[0];
        let date_end = date[1].split('T')[0];

        $('#current-time').html('JAM --:--');
        $('#current-po-a').html('-');
        $('#current-po-b').html('-');
        if (json.current_temp) {
            let current_temp = json.current_temp;

            $('#current-time').html('JAM ' + current_temp[0]);
            $('#current-po-a').html(current_temp[1]);
            $('#current-po-b').html(current_temp[2]);

            isPoAOverLimit = current_temp[1] > current_temp[4] || current_temp[1] < current_temp[5];
            isPoBOverLimit = current_temp[2] > current_temp[4] || current_temp[2] < current_temp[5];

            isPoAOverLimit ? setBlinkPo('a') : setUnBlinkPo('a');
            isPoBOverLimit ? setBlinkPo('b') : setUnBlinkPo('b');

        }
    });
}
renderInfo();

const setBlinkPo = (id) => {
    $(`#current-po-${id}`).attr({
        class: 'blinking'
    });
}

const setUnBlinkPo = (id) => {
    $(`#current-po-${id}`).attr({
        class: ''
    });
}

const activityWatcher = () => {

    let secondsSinceLastActivity = 0;

    let maxInactivity = 10;

    setInterval(() => {
        secondsSinceLastActivity++;
        if (secondsSinceLastActivity >= maxInactivity) {
            (secondsSinceLastActivity % 10) == 0 ? renderInfo() : '';

        }
    }, 1000);

    function activity() {
        secondsSinceLastActivity = 0;
    }

    var activityEvents = [
        'mousedown', 'mousemove', 'keydown', 'scroll', 'touchstart'
    ];

    activityEvents.forEach(function (eventName) {
        document.addEventListener(eventName, activity, true);
    });
}

activityWatcher();
