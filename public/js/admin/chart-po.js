google.charts.load('current', {
	'packages': ['corechart', 'line']
});

$('#date-input-internal').on('input', (e) => {
	$('#date-input-consumer').val($('#date-input-internal').val());
	renderChart();
});

$('#date-input-consumer').on('input', (e) => {
	$('#date-input-internal').val($('#date-input-consumer').val());
	renderChart();
});

$('#max-input-internal, #min-input-internal, #max-input-consumer, #min-input-consumer').on('blur', (e) => {
	renderChart(true);
});

function renderChart(isChangeMaxMinY = false) {
	let date = $('#date-input-internal').val();

	$.getJSON(`/administrator/chart/data/${date}`, function (json) {

		let max_y = json.max_y;
		let min_y = json.min_y;
		let max_y_temp = json.max_y_temp;
		let min_y_temp = json.min_y_temp;

		if (!isChangeMaxMinY) {

			$('#max-input-internal').val(json.max_y_temp);
			$('#max-input-internal').attr({
				min: json.max_y_temp,
			});

			$('#min-input-internal').val(json.min_y_temp);
			$('#min-input-internal').attr({
				max: json.min_y_temp,
			});

			$('#max-input-consumer').val(json.max_y);
			$('#max-input-consumer').attr({
				min: json.max_y,
			});

			$('#min-input-consumer').val(json.min_y);
			$('#min-input-consumer').attr({
				max: json.min_y,
			});

		} else {

			max_y_temp = parseInt($('#max-input-internal').val());
			min_y_temp = parseInt($('#min-input-internal').val());

			max_y = parseInt($('#max-input-consumer').val());
			min_y = parseInt($('#min-input-consumer').val());

		}

		let html_thead0 = `<tr><th>Hour</th>`;
		let html_tbody1 = '<tr><td>Po (A)</td>';
		let html_tbody2 = '<tr><td>Po (B)</td>';
		let html_tbody3 = '<tr><td>Target</td>';

		let len_data = json.chart_data;
		$.each(json.chart_data, (index, data) => {

			if (index == len_data)
				return false;

			let hour = data[0].split(":").join("<br>:<br>").replace(':', '....');

			html_thead0 += `<th>${hour}</th>`;
			html_tbody1 += `<td>${data[1]==null ? '-' : data[1]}</td>`;
			html_tbody2 += `<td>${data[2]==null ? '-' : data[2]}</td>`;
			html_tbody3 += `<td>${data[1]==null ? '-' : data[3]}</td>`;

		});

		html_thead0 += `</tr>`;
		html_tbody1 += `</tr>`;
		html_tbody2 += `</tr>`;
		html_tbody3 += `</tr>`;

		$('#render-thead').html(html_thead0);
		$('#render-tbody').html(html_tbody1 + html_tbody2 + html_tbody3);

        // Re-initiate for Po Temp
        html_thead0 = `<tr><th>Hour</th>`;
        html_tbody1 = '<tr><td>Po (A)</td>';
        html_tbody2 = '<tr><td>Po (B)</td>';
        html_tbody3 = '<tr><td>Target</td>';

        let len_data_temp = json.chart_data_temp;
        $.each(json.chart_data_temp, (index, data) => {

        	if (index == len_data_temp)
        		return false;

        	let hour = data[0].split(":").join("<br>:<br>").replace(':', '....');

        	html_thead0 += `<th>${hour}</th>`;
        	html_tbody1 += `<td>${data[1]==null ? '-' : data[1]}</td>`;
        	html_tbody2 += `<td>${data[2]==null ? '-' : data[2]}</td>`;
        	html_tbody3 += `<td>${data[1]==null ? '-' : data[3]}</td>`;

        });

        html_thead0 += `</tr>`;
        html_tbody1 += `</tr>`;
        html_tbody2 += `</tr>`;
        html_tbody3 += `</tr>`;

        $('#render-thead-temp').html(html_thead0);
        $('#render-tbody-temp').html(html_tbody1 + html_tbody2 + html_tbody3);

        let date = json.date;
        let date_start = date[0].split('T')[0];
        let date_end = date[1].split('T')[0];

        let mixin_option = {
        	title: `SPC Po, Production - ${date_start} 20:00 until ${date_end} 20:00`,
        	fontSize: 14,
        	chartArea: {
        		height: '100%',
        		width: '70%',
        		top: 48,
        		left: 48,
        		right: 114,
        		bottom: 48
        	},
        	width: '100%',
        	height: 500,
        	axes: {
        		x: {
        			0: {
        				side: 'bottom'
        			}
        		}
        	},
        	legend: 
                {
                    'position':'center',
                    'alignment':'center'
                },
        	series: {
        		0: {
        			color: '#43459d',
        			pointSize: 4
        		},
        		1: {
        			color: '#6f9654',
        			pointSize: 4
        		},
        		2: {
        			color: '#f1ca3a',
        			pointSize: 0
        		},
        		3: {
        			color: '#001100',
        			pointSize: 0
        		},
        		4: {
        			color: '#001100',
        			pointSize: 0
        		},
        		5: {
        			color: '#e2431e',
        			pointSize: 0
        		},
        		6: {
        			color: '#e2431e',
        			pointSize: 0
        		},
        		7: {
        			color: '#e2431e',
        			pointSize: 0
        		}

        	},
        	hAxis: {
        		slantedText: true,
        		slantedTextAngle: 90
        	}
        };

        let options = {
        	...mixin_option,
        	vAxis: {
        		scaleType: 'log',
        		viewWindowMode: 'explicit',
        		ticks: [...range(min_y, max_y)],
        		viewWindow: {
        			min: min_y,
        			max: max_y,
        		},
        		titleTextStyle: {
        			fontSize: 5
        		}
        	},

        };

        let options_temp = {
        	...mixin_option,
        	vAxis: {
        		scaleType: 'log',
        		viewWindowMode: 'explicit',
        		ticks: [...range(min_y_temp, max_y_temp)],
        		viewWindow: {
        			min: min_y_temp,
        			max: max_y_temp,
        		},
        		titleTextStyle: {
        			fontSize: 5
        		}
        	},

        };

        // Fault Tolerance Google Chart Error
        timeout = setInterval(() => {
        	if (google.visualization != undefined) {
        		google.charts.setOnLoadCallback(drawChart(json, options, date_start, date_end));
        		google.charts.setOnLoadCallback(drawChartTemp(json, options_temp, date_start, date_end));
        		clearInterval(timeout);
        	}
        }, 300);


    });
}

renderChart();

drawChartTemp = (json, options, date_start, date_end) => {

	let data = new google.visualization.DataTable();
	data.addColumn('string', 'Hour');
	data.addColumn('number', 'Po (A)');
	data.addColumn('number', 'Po (B)');
	data.addColumn('number', 'Target');
	data.addColumn('number', 'UCL');
	data.addColumn('number', 'LCL');
	data.addColumn('number', 'USL');
	data.addColumn('number', 'LSL');

	data.addRows(json.chart_data_temp);

	let chart = new google.visualization.LineChart(document.getElementById('po_temp'));

	chart.draw(data, google.charts.Line.convertOptions(options));

	let report_pdf_internal = document.getElementById('save-pdf-internal');
	google.visualization.events.addListener(chart, 'ready', () => {
		report_pdf_internal.disabled = false;
	});

	$('#save-pdf-internal').off();
	$('#save-pdf-internal').on('click', function () {

		var doc = new jsPDF({
			orientation: "landscape",
			format: 'c4'
		});

		var width = doc.internal.pageSize.getWidth();
		var height = doc.internal.pageSize.getHeight();

		doc.setFontType("bold");
		doc.text(150, 18, `SPC Po, Production Date ${date_start} 20:00 until ${date_end} 20:00`, 'center');


		doc.setFontType("normal");
		doc.setFontSize(10);

		doc.addImage(chart.getImageURI(), 10, 20, width - 10, 100);

		doc.autoTable({
			startY: 130,
			styles: {
				fontSize: 6
			},
			margin: {
				top: 10
			},
			html: '#table-po-temp'
		});

		doc.save(`SPC-Po-Production-${date_start}-20:00-until-${date_end}-20:00.pdf`);

	});
}

drawChart = (json, options, date_start, date_end) => {

	let data = new google.visualization.DataTable();
	data.addColumn('string', 'Hour');
	data.addColumn('number', 'Po (A)');
	data.addColumn('number', 'Po (B)');
	data.addColumn('number', 'Target');
	data.addColumn('number', 'UCL');
	data.addColumn('number', 'LCL');
	data.addColumn('number', 'USL');
	data.addColumn('number', 'LSL');

	data.addRows(json.chart_data);

	let chart = new google.visualization.LineChart(document.getElementById('po'));

	chart.draw(data, google.charts.Line.convertOptions(options));

	let report_pdf_consumer = document.getElementById('save-pdf-consumer');

	google.visualization.events.addListener(chart, 'ready', () => {
		report_pdf_consumer.disabled = false;
	});

	$('#save-pdf-consumer').off();
	$('#save-pdf-consumer').on('click', function () {

		var doc = new jsPDF({
			orientation: "landscape",
			format: 'c4'
		});

		var width = doc.internal.pageSize.getWidth();
		var height = doc.internal.pageSize.getHeight();

		doc.setFontType("bold");
		doc.text(150, 18, `SPC Po, Production Date ${date_start} 20:00 until ${date_end} 20:00`, 'center');

		doc.setFontType("normal");
		doc.setFontSize(10);

		doc.addImage(chart.getImageURI(), 10, 20, width - 10, 100);

		doc.autoTable({
			startY: 130,
			styles: {
				fontSize: 6
			},
			margin: {
				top: 10
			},
			html: '#table-po'
		});

		doc.save(`SPC-Po-Production-${date_start}-20:00-until-${date_end}-20:00.pdf`);

	});
}

function* range(start, end) {
	yield start;

	if (start === end) return;

	yield* range(start + 1, end);
}

const activityWatcher = () => {

	let secondsSinceLastActivity = 0;

	let maxInactivity = 10;

	setInterval(() => {
		secondsSinceLastActivity++;
		if (secondsSinceLastActivity >= maxInactivity) {
			(secondsSinceLastActivity % 10) == 0 ? renderChart(true) : '';

		}
	}, 1000);

	function activity() {
		secondsSinceLastActivity = 0;
	}

	var activityEvents = [
	'mousedown', 'mousemove', 'keydown', 'scroll', 'touchstart'
	];

	activityEvents.forEach(function (eventName) {
		document.addEventListener(eventName, activity, true);
	});
}

activityWatcher();