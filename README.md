## Boilerplate Admin (Laravel Admin)

Make an Administrator page in 5 minutes
-- Source From https://github.com/hexters/admin

![Example Image](https://github.com/hexters/admin/blob/master/examples/user.png?raw=true)

## Index 
- [Laravel Version](https://github.com/hexters/admin#laravel-version)
- [Note](https://github.com/hexters/admin#note)
- [Installation](https://github.com/hexters/admin#installation)
- [Manage Sidebar & Top Menu](https://github.com/hexters/admin#manage-sidebar--top-menu)
- [Create Datatables server](https://github.com/hexters/admin#create-datatables-server)
- [Role & Permission](https://github.com/hexters/admin#role--permission)
- [User Activity](https://github.com/hexters/admin#user-activity)
- [Blade Layout](https://github.com/hexters/admin#blade-layout)
- [Blade Components](https://github.com/hexters/admin/blob/master/readmes/components.md#blade-components)
  - [Card Component](https://github.com/hexters/admin/blob/master/readmes/components.md#card-component)
  - [Form Group Componenet](https://github.com/hexters/admin/blob/master/readmes/components.md#form-group-componenet)
- [Icons](https://github.com/hexters/admin#icons)
- [Custom Style](https://github.com/hexters/admin#custom-style)
- [Notification](https://github.com/hexters/admin#notification)
- [Admin Plugins](https://github.com/hexters/admin/blob/master/readmes/plugins.md)


## Laravel Version

|Version|Laravel|
|:-:|:-:|
| [v1.0.x](https://github.com/hexters/admin/blob/master/versions/1.0.md) | 7.x |
| Last version | 8.x |

## Note
Before using this package you must already have a login page or route login `route('login')` for your members, you can use [laravel/breeze](https://github.com/laravel/breeze), [larave/ui](https://github.com/laravel/ui) or [laravel jetstream](https://jetstream.laravel.com/1.x/introduction.html).

*For member pages, you should use a different guard from admin or vice versa.*

![Scheme](https://github.com/hexters/admin/blob/master/examples/scheme.png?raw=true)

## Installation
Clone Repository
```bash

$ git clone https://gitlab.com/aldo_k/belajar-laravel-01.git
```

Copy .env.example to .env and setting your laravel env
```bash

$ cp .env.example .env
```

Migrate database
```bash

$ php artisan migrate --seed
```


```

Installation finish, you can Access admin page in this link below.

```
http://localhost:8000/administrator
```

## Manage Sidebar & Top Menu
To add a menu open `app/Menus/sidebar.php` file and `top_right.php`

## Create Datatables server
Create datatables server to handle your list of data
```bash

$ php artisan make:datatables UserDataTables  --model=User

```
Example below
```php
. . .

use App\DataTables\UserDataTables;

class UserController extends Controller {

  . . .

  public function index() { 

    . . .

    return UserDataTables::view();

    // OR custom view and custom data

    return UserDataTables::view('your.custom.view', [ 
      /**
       * You can catch this data form blade or UserDatatables class 
       * via static property `self$data`
       */
      'foo' => 'bar'
    ]);

  }

. . .

```

## Role & Permission
Protect your module via Controller
```php

. . . 
class UserController extends Controller {

  . . .

  public function index() {

    admin()->allow(['administrator.account.admin.index']) // Call the gates based on menu `app/Menus/sidebar.php`

    . . .

    
```

For an other you can use `@can()` from blade or `auth()->user()->can()` more [Gates](https://laravel.com/docs/8.x/authorization#gates)

![Example Image](https://github.com/hexters/admin/blob/master/examples/gates.png?raw=true)

## User Activity

Add this trait `App\Models\AdminLogable` to all the models you want to monitor. Inspired by [haruncpi/laravel-user-activity](https://github.com/haruncpi/laravel-user-activity)
```php

. . .

use App\Models\AdminLogable;

class Role extends Model {
    
    use HasFactory, AdminLogable;

. . .

```

![Example Image](https://github.com/hexters/admin/blob/master/examples/activity.png?raw=true)

## System Log

![Example Image](https://github.com/hexters/admin/blob/master/examples/log.png?raw=true)

## Blade Layout
Admin layout

```html

  <x-template-layout>
    <x-slot name="title">Title Page</x-slot>
    <x-slot name="buttons">
      {-- Top Buttons --}      
    </x-slot>

    {-- Your content here --}    

  </x-template-layout>
  
```

![Example Image](https://github.com/hexters/admin/blob/master/examples/login.png?raw=true)

### Datatables Render
If you have a custom view for render data from [Datatables server](https://github.com/hexters/admin#create-datatables-server) you should call this component to render your table
```html
<x-template-datatables :fields="$fields" :options="$options" />
```
#### Attributes
|Attribute|value|require|
|-|-|-|
|`fields`|don't be changed the value should be still `$fields`|YES|
|`options`|don't be changed the value should be still `$options`|YES|

## Icons 
You can user [Fontawesome](https://fontawesome.com) or SVG file in `resources/assets/icons`, svg file retrieved from site [Heroicons](https://heroicons.com)
```php
  admin()->icon('fas fa-user')

  // OR

  admin()->icon('user')

  // OR 

  admin()->icon('somefolder.customicon') // resources/assets/icons/somefolder/customicon.svg

```

## Custom Style
Install node modules
```javascript
$ npm i jquery popper.js bootstrap @fortawesome/fontawesome-free datatables.net@1.10.21 datatables.net-bs4@1.10.21 vue --save

// OR

$ yarn add jquery popper.js bootstrap @fortawesome/fontawesome-free datatables.net@1.10.21 datatables.net-bs4@1.10.21 vue

```

Add this code to your  `webpack.mix.js` file
```javascript
mix.js('resources/js/admin/app.js', 'public/js/admin/app.js')
   .sass('resources/sass/admin/app.scss', 'public/css/admin/app.css');
```

## Custom Avatar 
By default admin uses gravatar. If you want to change it, add the `avatar_url` field to your` users` table and it must be a URL value.

Call avatar url below
```php
  $user->gravatar_url
  // out: gravatar url / your custom url
```

## Notification

By default notification has activated, you can disable notification with set value to `false`

```php
. . .

'notification' => false

. . .
```

Send notification
```php
admin()
  ->notification()
    ->setTitle('new Invoice')
    ->setLink('http://project.test/invoice/31eb6d58-3622-42a4-9206-d36e7a8d6c06')
    ->setDescription('Pay invoice #123455')
    ->setImageLink('http://porject.test/icon-invoice.ong') // optional
    ->setGates(['administrator.accounting', 'administrator.owner']) // optional
  ->send()

```
Notification required
|Option|Type|required|Note|
|-|-|:-:|-|
|`setTitle`|String|YES|-|
|`setLink`|String|YES|-|
|`setImageLink`|String|NO|-|
|`setDescription`|String|YES|-|
|`setGates`|Array|NO| default all gates |

### Listening For Notifications

Notifications will broadcast on a private channel formatted using a {notifiable}.{id} convention. View complete [Documentation](https://laravel.com/docs/8.x/notifications#listening-for-notifications)

```javascript

  // Change App.Models.User bas on config('admin.user')

  Echo.private('App.Models.User.' + userId)
    .notification((notification) => {
        console.log(notification.type);
    });
```
![Example Image](https://github.com/hexters/admin/blob/master/examples/notification.png?raw=true)

## Options
You can store the option for your application with the admin option, 
### Create or Update option
Data type of value is `String`, `Number` and `Array`
```php
  admin()->update_option('my-option', ['foo', 'bar', 'baz']);
  // out: boolean
```

### Get Option Value
```php
  admin()->get_option('my-option');
  // out: Array ['foo', 'bar', 'baz']
```

### Delete Option
```php
admin()->delete_option('my-option');
// out: boolean
```