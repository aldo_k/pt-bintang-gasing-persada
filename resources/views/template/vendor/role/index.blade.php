<x-template-layout>
  <x-slot name="title">Admin Role</x-slot>
    
  <x-template-datatables :fields="$fields" :options="$options" />

</x-template-layout>