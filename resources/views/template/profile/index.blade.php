<x-template-layout>
    <x-slot name="title">Profile</x-slot>
    <x-template-card class="col-6">

        <form action="{{ route('administrator.profile.store') }}" method="post">
            @csrf

            @include('administrator.user._partials._form', ['user' => auth()->user()])

            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    Update
                </button>
            </div>
        </form>
    </x-template-card>

</x-template-layout>
