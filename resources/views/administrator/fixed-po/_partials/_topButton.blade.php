@can('administrator.fixed-po.create')
  <a href="{{ route('administrator.fixed-po.create', ['back' => request()->fullUrl()]) }}" class="btn btn-sm btn-primary">Input Po Manual</a>
@endcan