<x-template-layout>
    <x-slot name="title">Create Po</x-slot>
    <x-template-card class="col-6">
        <x-slot name="flat">
            <div class="col-12 p-4">
                <form action="{{ route('administrator.fixed-po.store') }}" method="post">
                    @csrf

                    @include('administrator.fixed-po._partials._form', ['po' => (new App\Models\Po)])

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            Submit Po
                        </button>
                    </div>
                </form>
            </div>
        </x-slot>
    </x-template-card>

    <x-slot name="scripts">
        <script>
            $('#buyer_id').on('input', () => {
                $.getJSON(`/administrator/buyer/${$('#buyer_id').val()}`, function(data) {
                    $('#target').val(data.target);
                    $('#usl').val(data.usl);
                    $('#lsl').val(data.lsl);
                });
            });
        </script>
    </x-slot>
</x-template-layout>
