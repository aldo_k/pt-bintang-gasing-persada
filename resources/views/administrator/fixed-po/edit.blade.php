<x-template-layout>
    <x-slot name="title">Edit Po</x-slot>
    <x-template-card class="col-6">
        <x-slot name="flat">
            <div class="col-12 p-4">
                <form action="{{ route('administrator.fixed-po.update', $po->id) }}" method="post">
                    @csrf
                    @method('PUT')

                    @include('administrator.fixed-po._partials._form', ['po' => $po])

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            Update Po
                        </button>
                    </div>
                </form>
            </div>
        </x-slot>
    </x-template-card>

    <x-slot name="scripts">
        <script>

            $(document).ready(function() {
                $('#buyer_id').on('input', () => {
                    $.getJSON(`/administrator/buyer/${$('#buyer_id').val()}`, function(data) {
                        $('#target').val(data.target);
                        $('#usl').val(data.usl);
                        $('#lsl').val(data.lsl);
                    });
                });
            });

        </script>
    </x-slot>

</x-template-layout>
