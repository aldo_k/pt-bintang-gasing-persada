<x-template-layout>
    <x-slot name="title">Edit Buyer</x-slot>
    <x-template-card class="col-6">
        <x-slot name="flat">
            <div class="col-12 p-4">
                <form action="{{ route('administrator.buyer.update', $buyer->id) }}" method="post">
                    @csrf
                    @method('PUT')

                    @include('administrator.buyer._partials._form', ['buyer' => $buyer])

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            Update Buyer
                        </button>
                    </div>
                </form>
            </div>
        </x-slot>
    </x-template-card>
</x-template-layout>
