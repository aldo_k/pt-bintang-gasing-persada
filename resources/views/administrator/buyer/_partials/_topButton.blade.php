@can('administrator.buyer.create')
  <a href="{{ route('administrator.buyer.create', ['back' => request()->fullUrl()]) }}" class="btn btn-sm btn-primary">Create Buyer</a>
@endcan