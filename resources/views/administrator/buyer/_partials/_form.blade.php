<x-template-form-group name="name" label="Buyer Name *">
  <x-slot name="prepend">
    {!! admin()->icon('user-circle') !!}
  </x-slot>
  <input type="text" placeholder="Buyer Name" class="form-control" name="name" id="name" required value="{{ old('name', $buyer->name) }}">
</x-template-form-group>

<x-template-form-group name="target" label="Target *">
  <x-slot name="prepend">
    {!! admin()->icon('desktop-computer') !!}
  </x-slot>
  <input type="number" placeholder="Target" class="form-control" name="target" id="target" required value="{{ old('name', $buyer->target) }}" max="99" min="1">
</x-template-form-group>

<x-template-form-group name="target" label="USL (Default) *">
    <x-slot name="prepend">
        {!! admin()->icon('desktop-computer') !!}
    </x-slot>
    <input type="number" placeholder="USL (Default)" class="form-control" name="usl" id="usl" required value="{{ old('name', $buyer->usl) }}" max="99" min="1">
</x-template-form-group>

<x-template-form-group name="target" label="LSL (Default) *">
    <x-slot name="prepend">
        {!! admin()->icon('desktop-computer') !!}
    </x-slot>
    <input type="number" placeholder="LSL (Default)" class="form-control" name="lsl" id="lsl" required value="{{ old('name', $buyer->lsl) }}" max="99" min="1">
</x-template-form-group>
