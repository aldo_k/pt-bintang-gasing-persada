<x-template-layout>
  <x-slot name="title">Create Buyer</x-slot>
  <x-template-card class="col-6">
    <x-slot name="flat">
      <div class="col-12 p-4">
        <form action="{{ route('administrator.buyer.store') }}" method="post">
          @csrf

          @include('administrator.buyer._partials._form', ['buyer' => (new App\Models\Buyer)])


          <div class="text-right">
            <button type="submit" class="btn btn-primary">
              Submit Buyer
            </button>
          </div>
        </form>
      </div>
    </x-slot>

  </x-template-card>

</x-template-layout>