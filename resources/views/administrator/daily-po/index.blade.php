<x-template-layout>
    <x-slot name="title">Daily Po</x-slot>

    <x-slot name="styles">
        <style>
            .table td,
            .table th {
                vertical-align: middle;
            }

            .table-mini td {
                padding: .1rem;
            }

            .form-control {
                text-align: center;
                display: block;
                width: 100%;
                height: calc(1.6em + .75rem + 2px);
                padding: .375rem .75rem;
                font-size: 1.1rem;
                font-weight: 400;
                line-height: 1.6;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid transparent;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
            }

            .blinking {
                animation: blinkingText 1.2s infinite;
            }

            @keyframes blinkingText {
                0% {
                    color: #000;
                }

                49% {
                    color: #000;
                }

                60% {
                    color: transparent;
                }

                99% {
                    color: transparent;
                }

                100% {
                    color: #000;
                }
            }

        </style>
    </x-slot>
    <div class="row">
        <x-template-card class="col-12">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="row">
                    <div class="col-6 table-responsive text-center mt-4">
                        <table id="table-po-temp" class="table table-sm table-hover table-bordered rotate-table-grid" style="font-size:32pt; line-height:1.05">
                            <thead>
                                <tr>
                                    <th colspan="2" class="bg-warning" id="current-time">JAM --:--</th>
                                </tr>
                                <tr class="bg-success">
                                    <th>DRYER A</th>
                                    <th>DRYER B</th>
                                </tr>
                            </thead>
                            <tbody height="438px">
                                <tr class="bg-default" style="font-size:188pt; line-height:1.05">
                                    <td id="current-po-a">-</td>
                                    <td id="current-po-b">-</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-6 table-responsive text-center mt-4">
                        <form action="{{ route('administrator.daily-po.store') }}" method="post">
                            @csrf
                            <table id="table-po-temp" class="table table-mini table-hover table-bordered rotate-table-grid" style="font-size:22pt; line-height:1.05">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="bg-warning" id="current-date">PROD 27 November 2021</th>
                                    </tr>
                                    <tr class="bg-success">
                                        <th width="30%">SHIFT</th>
                                        <th width="35%">MALAM</th>
                                        <th width="35%">PAGI</th>
                                    </tr>

                                </thead>
                                <tbody style="font-size:0.8em; line-height:1.05;">
                                    <tr class="bg-success">
                                        <td>PROD</td>
                                        <td>
                                            <input type="text" class="form-control bg-success" name="cooper_a" id="cooper_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control bg-success" name="cooper_b" id="cooper_b" placeholder="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>BLOK</td>
                                        <td>
                                            <input type="hidden" name="date_report" id="date_report">
                                            <input type="text" class="form-control" name="block_a" id="block_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="block_b" id="block_b" placeholder="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>DRC</td>
                                        <td>
                                            <input type="text" class="form-control" name="drc_a" id="drc_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="drc_b" id="drc_b" placeholder="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PO</td>
                                        <td>
                                            <input type="text" class="form-control" name="po_a" id="po_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="po_b" id="po_b" placeholder="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PRI</td>
                                        <td>
                                            <input type="text" class="form-control" name="pri_a" id="pri_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="pri_b" id="pri_b" placeholder="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>UMUR JEMUR</td>
                                        <td>
                                            <input type="text" class="form-control" name="umur_jemur_a" id="umur_jemur_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="umur_jemur_b" id="umur_jemur_b" placeholder="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>KAMAR</td>
                                        <td>
                                            <input type="text" class="form-control" name="kamar_a" id="kamar_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="kamar_b" id="kamar_b" placeholder="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>TARGET / RANGE</td>
                                        <td>
                                            <input type="text" class="form-control" name="target_a" id="target_a" placeholder="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="target_b" id="target_b" placeholder="">
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3">
                                            <button type="submit" id="submit-data" class="btn btn-primary">Submit Data</button>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </form>
                    </div>

                    <div class="col-12 table-responsive mt-4">
                        <table id="table-po-temp" class="table table-sm table-hover table-bordered table-striped rotate-table-grid" style="font-size:9pt; line-height:1.05">
                            <thead id="render-thead-temp"></thead>
                            <tbody id="render-tbody-temp"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </x-template-card>
    </div>

    <x-slot name="scripts">
        <script type="text/javascript" src="{{ asset('js/admin/daily-po.js') }}"></script>
    </x-slot>
</x-template-layout>
