<x-template-layout>
    <x-slot name="title">Dashboard</x-slot>

    <div class="row">
        <x-template-card class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" onclick="renderChart()">Report Internal</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="consumer-tab" data-toggle="tab" href="#consumer" role="tab" aria-controls="consumer" aria-selected="true" onclick="renderChart()">Report Consumer</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-6 mt-4">
                            <a href="javascript:void(0);" id="start-alarm" class="btn btn-sm btn-danger mx-1 d-none" onclick="startAlarm()">
                                <i class="fas fa-bell mr-1"></i> Start Alarm
                            </a>
                            <a href="javascript:void(0);" id="stop-alarm" class="btn btn-sm btn-dark mx-1" onclick="stopAlarm()">
                                <i class="fas fa-bell-slash mr-1"></i> Stop Alarm
                            </a>
                            <audio id="sirine1" type="audio/mp3" src="{{ asset('sound1.mp3') }}" loop>
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                        <div class="col-12">
                            <div id="po_temp" width="100%"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="consumer" role="tabpanel" aria-labelledby="consumer-tab">
                    <div class="row">
                        <div class="col-12">
                            <div id="po" width="100%"></div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="current-time">
        </x-template-card>
    </div>

    <x-slot name="scripts">
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="{{ asset('js/admin/dashboard.js') }}"></script>
    </x-slot>
</x-template-layout>
