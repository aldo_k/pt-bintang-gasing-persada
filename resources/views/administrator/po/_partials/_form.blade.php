<x-template-form-group name="time" label="Time Input Po *">
    <x-slot name="prepend">
        {!! admin()->icon('calendar') !!}
    </x-slot>

    <input type="text" placeholder="Time" class="form-control" id="report_time" required value="{{ $po->report_time ?: $current_time }}" readonly>


</x-template-form-group>

@if (isset($buyers))
<x-template-form-group name="buyer_id" label="(Target) Buyer *">
    <x-slot name="prepend">
        {!! admin()->icon('user-circle') !!}
    </x-slot>

    <select name="buyer_id" id="buyer_id" class="form-control border-0" required>
        <option value="">- Select Buyer -</option>
        @foreach ($buyers as $buyer)
        <option value="{{ $buyer->id }}" {{ isset($po->target) && $po->target == $buyer->target ? 'selected' : '' }}>( Target {{ $buyer->target }} ) {{ $buyer->name }} </option>


        @endforeach
    </select>
</x-template-form-group>

<x-template-form-group name="target" label="USL (Default) *">
    <x-slot name="prepend">
        {!! admin()->icon('desktop-computer') !!}
    </x-slot>
    <input type="hidden" name="target" id="target" readonly required value="{{ old('name', $po->target) }}" max="99" min="1">
    <input type="number" placeholder="USL (Default)" class="form-control" name="usl" id="usl"  required value="{{ old('name', $po->usl) }}" max="99" min="1">
</x-template-form-group>

<x-template-form-group name="target" label="LSL (Default) *">
    <x-slot name="prepend">
        {!! admin()->icon('desktop-computer') !!}
    </x-slot>
    <input type="number" placeholder="LSL (Default)" class="form-control" name="lsl" id="lsl"  required value="{{ old('name', $po->lsl) }}" max="99" min="1">

</x-template-form-group>
@endif

<x-template-form-group name="po_a" label="Po (A) *">
    <x-slot name="prepend">
        {!! admin()->icon('chart-bar') !!}
    </x-slot>

    <input type="number" placeholder="Po (A)" class="form-control" name="po_a" id="po_a" value="{{ old('name', $po->po_a) }}" max="99" min="1">
</x-template-form-group>

<x-template-form-group name="po_b" label="Po (B) *">
    <x-slot name="prepend">
        {!! admin()->icon('chart-bar') !!}
    </x-slot>

    <input type="number" placeholder="Po (B)" class="form-control" name="po_b" id="po_b" value="{{ old('name', $po->po_b) }}" max="99" min="1">
</x-template-form-group>
