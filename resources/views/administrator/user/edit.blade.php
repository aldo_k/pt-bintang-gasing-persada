<x-template-layout>
  <x-slot name="title">Edit User</x-slot>
  <x-template-card class="col-6">
    <x-slot name="flat">
      <div class="col-12 p-4">
        <form action="{{ route('administrator.account.admin.update', $user->id) }}" method="post">
          @csrf 
          @method('PUT')
            
          @include('administrator.user._partials._form', ['user' => $user])

          <div class="text-right">
            <button type="submit" class="btn btn-primary">
              Update User
            </button>
          </div>
        </form>
      </div>
    </x-slot>
  </x-template-card>
</x-template-layout>