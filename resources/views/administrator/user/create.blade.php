<x-template-layout>
  <x-slot name="title">Create User</x-slot>
  <x-template-card class="col-6">
    <x-slot name="flat">
      <div class="col-12 p-4">
        <form action="{{ route('administrator.account.admin.store') }}" method="post">
          @csrf

          @include('administrator.user._partials._form', ['user' => app(config('admin.user', App\Models\User::class))])

          <div class="text-right">
            <button type="submit" class="btn btn-primary">
              Submit User
            </button>
          </div>
        </form>
      </div>
    </x-slot>

  </x-template-card>

</x-template-layout>