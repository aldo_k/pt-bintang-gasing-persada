<x-template-layout>
    <x-slot name="title">Create Role</x-slot>

    <x-template-card class="col-6">
        <form action="{{ route('administrator.access.role.store') }}" method="post">
            @csrf

            @include('administrator.role._partials._form', ['role' => (new App\Models\Role)])

            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    Submit Role
                </button>
            </div>
        </form>
    </x-template-card>


</x-template-layout>
