<x-template-layout>
    <x-slot name="title">Chart SPC Po</x-slot>

    <div class="row">
        <x-template-card class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" onclick="renderChart(true)">Report Internal</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" onclick="renderChart(true)">Report Consumer</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-4 mt-4">
                            <a href="javascript:void(0);" id="save-pdf-internal" class="btn btn-sm mx-1 btn-danger" disabled="disabled">
                                <i class="fas fa-file-pdf mr-1"></i> Report Internal
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm mx-1 btn-success" onclick="return ExcellentExport.convert({ anchor: this, filename: 'Export Po Internal', format: 'xlsx' }, [{ name: 'Export Po Internal', fixValue: (value, row, col) => { let v = value.replace('<br>....<br>', ':'); let strippedString=v.replace(/(<([^>]+)>)/gi, ''); return strippedString;}, from: { table: 'table-po-temp', } }]);">

                                <i class="fas fa-file-excel mr-1"></i> Export Excel
                            </a>
                        </div>
                        <div class="col-8 mt-4">
                            <form class="form-inline float-right">
                                <div class="form-group">
                                    <label for="min-input-internal">Min :</label>
                                    <input type="number" id="min-input-internal" class="form-control form-control-sm mx-1 w-50">
                                </div>
                                <div class="form-group">
                                    <label for="max-input-internal">Max :</label>
                                    <input type="number" id="max-input-internal" class="form-control form-control-sm mx-1 w-50">
                                </div>
                                <div class="form-group">
                                    <label for="date-input-internal">Date :</label>
                                    <input type="date" id="date-input-internal" class="form-control form-control-sm mx-1" value="{{ $date }}">
                                </div>
                            </form>
                        </div>

                        <div class="col-12">
                            <div id="po_temp" width="100%"></div>
                        </div>

                        <div class="col-12 table-responsive mt-4">
                            <table id="table-po-temp" class="table table-sm table-hover table-striped rotate-table-grid" style="font-size:9pt; line-height:1.05">
                                <thead id="render-thead-temp"></thead>
                                <tbody id="render-tbody-temp"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                        <div class="col-4 mt-4">
                            <a href="javascript:void(0);" id="save-pdf-consumer" class="btn btn-sm btn-danger" disabled="disabled">
                                <i class="fas fa-file-pdf mr-1"></i> Report Consumer
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm mx-1 btn-success" onclick="return ExcellentExport.convert({ anchor: this, filename: 'Export Po Consumer', format: 'xlsx' }, [{ name: 'Export Po Consumer', fixValue: (value, row, col) => { let v = value.replace('<br>....<br>', ':'); let strippedString=v.replace(/(<([^>]+)>)/gi, ''); return strippedString;}, from: { table: 'table-po', } }]);">

                                <i class="fas fa-file-excel mr-1"></i> Export Excel
                            </a>
                        </div>
                        <div class="col-8 mt-4">
                            <form class="form-inline float-right">
                                <div class="form-group">
                                    <label for="min-input-consumer">Min :</label>
                                    <input type="number" id="min-input-consumer" class="form-control form-control-sm mx-1 w-50">
                                </div>
                                <div class="form-group">
                                    <label for="max-input-consumer">Max :</label>
                                    <input type="number" id="max-input-consumer" class="form-control form-control-sm mx-1 w-50">
                                </div>
                                <div class="form-group">
                                    <label for="date-input-consumer">Date :</label>
                                    <input type="date" id="date-input-consumer" class="form-control form-control-sm mx-1" value="{{ $date }}">
                                </div>
                            </form>
                        </div>

                        <div class="col-12">
                            <div id="po" width="100%"></div>
                        </div>

                        <div class="col-12 table-responsive mt-4">
                            <table id="table-po" class="table table-sm table-hover table-striped rotate-table-grid" style="font-size:9pt; line-height:1.05">
                                <thead id="render-thead"></thead>
                                <tbody id="render-tbody"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="current-time">
            <input type="hidden" id="current-time-silent-alarm">
        </x-template-card>
    </div>

    <x-slot name="scripts">
        <script type="text/javascript" src="{{ asset('js/jspdf/dist/jspdf.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jspdf-autotable/dist/jspdf.plugin.autotable.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/exportexcel/excellentexport.js') }}"></script>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="{{ asset('js/admin/chart-po.js') }}"></script>
    </x-slot>
</x-template-layout>
