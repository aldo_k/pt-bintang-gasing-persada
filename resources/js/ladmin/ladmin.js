window.dt = require('datatables.net').default;

/**
 * Button togle sidebar
 */
const content = document.querySelector('.admin-container');
const btnToggle = document.querySelector('.admin-sidebar-toggle');

if(window.localStorage.getItem('side_bar_class')) {
  content.className = window.localStorage.getItem('side_bar_class');
}

btnToggle.addEventListener('click', function () {
  if(! content.classList.contains('admin-container-full') ) {
    content.className = 'admin-container admin-container-full';
    save_position('admin-container admin-container-full');
  } else {
    content.className = 'admin-container';
    save_position('admin-container');
  }
});

if(window.innerWidth <= 575.98) {
  save_position('admin-container admin-container-full');
  content.className = 'admin-container admin-container-full';
}

window.addEventListener('resize', function () {
  if(window.innerWidth <= 575.98) {
    save_position('admin-container admin-container-full');
    content.className = 'admin-container admin-container-full';
  }
});

function save_position(className) {
  window.localStorage.setItem('side_bar_class', className);
}

let menus = document.querySelectorAll('.admin-sidebar ul li');
menus.forEach(el => {
  if(el.querySelector('ul')) {
    let span = document.createElement('span');
    span.className = 'caret float-right mt-2';
    let a = el.querySelector('a');
    if(!a.querySelector('.badge')) {
      a.appendChild(span);
    }
  }

  let a = el.querySelector('a');
  a.addEventListener('click', function() {
    el.classList.toggle('show');
  })
});

/**
 * Menu active detection
 */
  let adminSidebar = document.querySelector('.admin-sidebar');
  let activeMenus = adminSidebar.querySelector('li.active');

  setActiveMenu = (menu) => {
    let parent = menu.parentNode;

    if(parent.classList.contains('admin-sidebar')) {
      return;
    }

    if(parent.nodeName === 'LI') {
      parent.classList.add('show');
    }

    setActiveMenu(parent);

  }

  if(activeMenus) {
    setActiveMenu(activeMenus);
  }

  /**
   * Datatables render
   */
  let ladmiDatatables = document.querySelectorAll('.admin-datatables');
  ladmiDatatables.forEach((el) => {
    let options = el.getAttribute('data-options');
    options = JSON.parse(options);
    $(el).DataTable({
      language: {
        search: '',
        searchPlaceholder: 'Search...'
      },
      ...options
    });
  });


/**
 * Jquery Section
 */
$(function() {

  $('.admin-datatable-base').each(function() {
    $(this).DataTable();
  });

  $('.permission-checkbox').on('click', function () {
    $(this).parent().find('li .permission-checkbox').prop('checked', $(this).is(':checked'));
    var sibs = false;
    $(this).closest('ul').children('li').each(function () {
        if ($('.permission-checkbox', this).is(':checked')) sibs = true;
    })
    $(this).parents('ul').prev().prop('checked', sibs);
  });

  $('.admin-notification-link').on('click', function(e) {
    e.preventDefault();
    const route = $(this).attr('href');
    const link = $(this).data('link');

    $.post(route, { _method : 'PUT' }, function() {
      window.location.href = link;
    }).catch(e => {
      if(e.response.status > 200) {
        alert(e.response.data.message);
      }
    });

  });

  $('[data-toggle="tooltip"]').tooltip();
});

window.sidebarMenu = function(val) {
  const content = document.querySelector('.admin-container');
  if(val === 'show') {
    content.classList.remove('admin-container-full');
  } else if(val === 'hide') {
    content.classList.add('admin-container-full');
  }
}